﻿'$$$GSEScript$$$
'GSE rule: rulePreProcessing; GSEVersion: 10.40.0.5
'#include "actionAutoVoicemailV2.vbs"
Function rulePreProcessing(ByRef rInputParams)
  PBXScriptOutputTrace "Begin GSE Script (Rule: rulePreProcessing)"
  PBXScriptOutputTrace "GSEVersion: 10.40.0.5"
  Dim szState, retVal, rcDummy, bWasDisconnect, UseExit, bRuleResult
  bRuleResult = False
  szState = "Start0"
  bWasDisconnect = False
  
  Do
    Select Case szState
    
      '////////////////////////
      Case "Start0"
        PBXScriptOutputTrace "  case [Start0]"
        retVal = gseStart()
        Select Case retVal
          Case gseStateStarted
            szState = "Evaluate4"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "End1"
        PBXScriptOutputTrace "  case [End1]"
        bRuleResult = True
        Exit Do
      
      '////////////////////////
      Case "Skip2"
        PBXScriptOutputTrace "  case [Skip2]"
        bRuleResult = False
        Exit Do
      
      '////////////////////////
      Case "Disconnect3"
        PBXScriptOutputTrace "  case [Disconnect3]"
        If (Not bWasDisconnect) Then
          szState = "End1"
          bWasDisconnect = True
        Else
          PBXScriptOutputTrace "GSE anti-LOOP script executed"
          bRuleResult = True
          Exit Do
        End If
      
      '////////////////////////
      Case "Evaluate4"
        PBXScriptOutputTrace "  case [Evaluate4]"
        PBXScriptOutputTrace "IpPbx.CallingNumber=CalledNumber()"
        retVal = gseEvaluateEx(IpPbx.CallingNumber=CalledNumber())
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "RunEditorAction5"
          Case gseStateEvaluateNoMatch
            szState = "Skip2"
          Case gseStateDisconnected
            szState = "Disconnect3"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "RunEditorAction5"
        PBXScriptOutputTrace "  case [RunEditorAction5]"
        Dim rInParams_6_actionAutoVoicemailV2
        Set rInParams_6_actionAutoVoicemailV2 = new GSEParamList
        rInParams_6_actionAutoVoicemailV2.Count = 10
        rInParams_6_actionAutoVoicemailV2.AddParam "femaleVoice", "No"
        rInParams_6_actionAutoVoicemailV2.AddParam "avLanguage", "GB"
        rInParams_6_actionAutoVoicemailV2.AddParam "shortMessage", "Yes"
        rInParams_6_actionAutoVoicemailV2.AddParam "Reason1", "Training"
        rInParams_6_actionAutoVoicemailV2.AddParam "Reason2", "Away"
        rInParams_6_actionAutoVoicemailV2.AddParam "Reason3", "Lunch"
        rInParams_6_actionAutoVoicemailV2.AddParam "Reason4", "Meeting"
        rInParams_6_actionAutoVoicemailV2.AddParam "Reason5", "Holiday"
        rInParams_6_actionAutoVoicemailV2.AddParam "Reason6", "Out"
        rInParams_6_actionAutoVoicemailV2.AddParam "Reason7", "Unavailable"
        
        actionAutoVoicemailV2 rInParams_6_actionAutoVoicemailV2
        
        Select Case CStr(rInParams_6_actionAutoVoicemailV2.m_FunctionResult)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateExecuted
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateExecuted
            szState = "End1"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect3"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case Else
        'Something wrong
        Exit Do
    End Select
    
  Loop
  
  
  rulePreProcessing = bRuleResult
  PBXScriptOutputTrace "Rule result: " & bRuleResult
  PBXScriptOutputTrace "End GSE Script (Rule: rulePreProcessing)"
End Function
'' SIG '' Begin Signature
'' SIG '' BSMLUWHOLYNDBVDONWJNPSBLTCHKJUMALDDFKWWKGMOOWDIMPUCNNWQJPTUX
'' SIG '' RKEKFAVDOUEMUWPQMPRWKYLKKVNZZTOLDZEOJAXUPGLKVQOCHIMYAYQCGFLX
'' SIG '' PGTOQAYMERKJSNKNUWYDGKSANIMKLXBQLKOSGQHZENMZPGSFSMIKVNTNRFSK
'' SIG '' YHZTWSRKAGKQKKPLKJMKLVIDGSJWVOGNAOMMMHNSNNKXVNPQCMKMXVJXWKAO
'' SIG '' OTDQLGOGLVQQIWRISOGOMGJARLRIZHWKLMSPEFZYRQHLKGKPJUAMCSQLGWRD
'' SIG '' AJQEGPUFBBHBZPLBRWTIGIVGIAKHNHLDCTHOOUWWPRMCLUSMLISTPGLUGXEU
'' SIG '' PVUPDTKNKHIBSUUUKFMKWOUEVDJGEJLVVGYKZWWHLUTRPOKAAMXOPBWURDWP
'' SIG '' JNKXWWOGNBULLILKOHFPKHOIMIYENOFAJMQNKWPBOOCWXTVFFQKHBQCLHLNC
'' SIG '' MNILYRKDHKCRKJZYXUQZEPULZPSEMRKRKULHESYWONQJQSUKREOYSLBEMEWO
'' SIG '' MLLLKTLVCIOMJTNKTMOBLEOGQMUOCSLWOAOWQWMOLVAMWWNJXCYTBAOUHQNF
'' SIG '' YXBJVETFIZNTNLLDDTYUYLWQKQUBOWOABFXQZHUAMBVYXLKFTLEVUNJDOKNX
'' SIG '' PZIKALGMETXQPPXJYAPMOQYWJGKUDNMKSCONMMELFTYOHNMNLWGFXSHLKYOR
'' SIG '' UXCKCSLVZLLSAOVCIEJONHOSYFEGMDUAMAUVHOZNIJCZPECNKJONGWMNEDZB
'' SIG '' CMMCRJLPYTRYNIBPBOMBMFKYQZEQCKKXNOMOGLRXZPNPRFKMZHIQIMHPFOPD
'' SIG '' KQEEVJROGGOLJUOOCMVSNPFFENVKNKZJDKNXBFKOCSWXUNZTAYLNEWEIVPLD
'' SIG '' XMQVXWMTLISONWNJAFIPOPGCPICMQAYZXPOFMFBBDCTBRORWLPEWPPPPNQLL
'' SIG '' WMYNRPNVIJBFDYNKMYVPLGUICUAYMMDHRBPMULLJUFXIMVNOMJZOMTQUOLRL
'' SIG '' CNZS
'' SIG '' End Signature