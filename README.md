# README #

Full instructions for installation and use can be found in the readme document in the download

### What is this repository for? ###

* Swyx AutoVoicemail
* Version 2.2 (155)

### Additional Language Packs ###

* Using the included XLS sheet you will find the English script for the wav files and their correct filenames to be used in this project
* Additional Language sets can be created if you send the completed spreadsheet to me.

### Who do I talk to? ###

* Repo owner