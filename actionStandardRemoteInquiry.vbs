﻿'$$$GSEScript$$$
'GSE action: actionStandardRemoteInquiry; GSEVersion: 10.40.0.5
'#include "actionRedirection.vbs"
Function UserEmailAddress(ByVal UserAddr)

	dim oPBXConfig, oUSers, oUser

	set oPBXConfig = PBXScript.CreateObject("IpPBxSrv.PBXConfig")

	oPBXConfig.Initialize PBXUser

	set oUsers = oPBXConfig.GetUserByAddress(UserAddr)

	PBXScriptOutputTrace "Number of matching Users: " & oUsers.Count

	for Each oUser in oUsers
		PBXScriptOutputTrace "User: " & oUser.UserID
		PBXScriptOutputTrace "Name: " & oUser.Name
		PBXScriptOutputTrace "EMailAddress: " & oUser.EMailAddress
		UserEmailAddress = oUser.EMailAddress
		PBXScriptOutputTrace "Folder: " & oUser.DataFolder
		PBXScriptOutputTrace "State: " & oUser.State
		dim n
		dim szNumbers
		szNumbers = ""
		for Each n in oUser.Numbers
			if szNumbers="" then
				szNumbers = n
			else
				szNumbers = szNumbers & ", " & n
			end if
		next
		PBXScriptOutputTrace "Numbers: " & szNumbers
	next
End Function

Function GetUserName(ByVal UserAddr)

	dim oPBXConfig, oUSers, oUser

	set oPBXConfig = PBXScript.CreateObject("IpPBxSrv.PBXConfig")

	oPBXConfig.Initialize PBXUser

	set oUsers = oPBXConfig.GetUserByAddress(UserAddr)

	PBXScriptOutputTrace "Number of matching Users: " & oUsers.Count

	for Each oUser in oUsers
		PBXScriptOutputTrace "Name: " & oUser.Name
		GetUserName = oUser.Name
	next
End Function

Function GetEmailAddrFromCallerID(ByVal szClrID, ByVal szFrom)
  If szClrID = "" Then
    GetEmailAddrFromCallerID = szFrom
  Else
    GetEmailAddrFromCallerID = UserEmailAddress(szClrID)
  End If
End Function

Sub GetAddrFromFullAddrStr(ByRef szFrom)
Dim i,j
  i = instr(szFrom,"<")
  j = instr(szFrom,">")
  If (i <> 0) AND (j <> 0) AND (j > (i+1)) Then szFrom = Mid(szFrom, i+1, j-i-1)
  PBXScriptOutputTrace "GetAddrFromFullAddrStr = " & szFrom
End Sub

Function OnlyFromOriginator(ByVal bUseFromCriteria)
  If (bUseFromCriteria = "") OR (bUseFromCriteria = False) Then OnlyFromOriginator = ""
  If (bUseFromCriteria = True) Then OnlyFromOriginator = VoicemailOriginatorEMailAddress()
  PBXScriptOutputTrace "OnlyFromOriginator = " & OnlyFromOriginator
End Function

Sub DeleteWavs(ByVal szFromWav, ByVal szSubjectWav, ByVal szBodyWav, ByVal szBody1Wav)
Dim fs, nStartTime

  PBXScriptOutputTrace "--> DeleteWavs " & szFromWav & " " & szSubjectWav & " " & szBodyWav & " " & szBody1Wav

  Set fs = CreateObject("Scripting.FileSystemObject")

  nStartTime = Now
  
  On Error Resume Next
  
  Do While DateDiff("s", nStartTime, Now) < 15
    If fs.FileExists(szFromWav) Then fs.DeleteFile(szFromWav)
    If Err.Description = "" Then Exit Do
    Err.Clear
    PBXScript.Sleep(500)
  Loop
  
  nStartTime = Now
  
  Do While DateDiff("s", nStartTime, Now) < 15
    If fs.FileExists(szSubjectWav) Then fs.DeleteFile(szSubjectWav)
    If Err.Description = "" Then Exit Do
    Err.Clear
    PBXScript.Sleep(500)
  Loop
  
  nStartTime = Now
  
  Do While DateDiff("s", nStartTime, Now) < 15
    If fs.FileExists(szBodyWav) Then fs.DeleteFile(szBodyWav)
    If Err.Description = "" Then Exit Do
    Err.Clear
    PBXScript.Sleep(500)
  Loop
  
  nStartTime = Now
  
  Do While DateDiff("s", nStartTime, Now) < 15
    If fs.FileExists(szBody1Wav) Then fs.DeleteFile(szBody1Wav)
    If Err.Description = "" Then Exit Do
    Err.Clear
    PBXScript.Sleep(500)
  Loop
  
  PBXScriptOutputTrace "<-- DeleteWavs"
End Sub

Function SelectMainMenuSound()
    SelectMainMenuSound = "Announcements/RI_main_menu_email_no_TTS.wav"
End Function

Function ExtractCallCDRID(ByVal strPostDial)
  Dim nPos
  nPos = InStr(strPostDial, "#")
  If (nPos > 0) Then
    ExtractCallCDRID = Mid(strPostDial, 1, nPos-1)
  Else
    ExtractCallCDRID = strPostDial
  End If
End Function


Sub actionStandardRemoteInquiry(ByRef rInputParams)
  PBXScriptOutputTrace "Begin GSE Script (Action: actionStandardRemoteInquiry)"
  PBXScriptOutputTrace "GSEVersion: 10.40.0.5"
  Dim szState, retVal, rcDummy, bWasDisconnect, UseExit
  szState = "Start0"
  bWasDisconnect = False
  
  Dim DTMF_SMENU, szMailCallerID, szUserName, timeEntry, bLast, szServerName, szMainMenuSound, bUseSSL, nStartTime, szUserEmailAddress, nStartDateTime, szCallCDRID, nOldVoiceMailCount, szPIN, fnResults, nNewVoiceMailCount, mGetAllEmails, bUseFromCriteria, DTMF_INPUT, nMainMenuLoopCnt, StatusOption, szMenuWav, nDiffTime, iPinRetries, mGetAllVoicemails2, DTMF_MMENU, bUsePINPrompt, szMailCallerName, szPassword, szInput, mGetAllVoicemailsAll, szForwardExtension, mGetNewVoicemails, writeTime, dateEntry, szMailTime, bPINok, writeDate, szFrom, bEnabled, bIsVoicemail, szMailFolder, nMailLength, szMailDate, szVMailWav
  
  DTMF_SMENU = ""
  szMailCallerID = ""
  szUserName = ""
  timeEntry = ""
  bLast = ""
  szServerName = ""
  szMainMenuSound = "Announcements/RI_main_menu_email_no_TTS.wav"
  bUseSSL = ""
  nStartTime = ""
  szUserEmailAddress = ""
  nStartDateTime = ""
  szCallCDRID = ""
  nOldVoiceMailCount = ""
  szPIN = ""
  fnResults = ""
  nNewVoiceMailCount = ""
  mGetAllEmails = ""
  bUseFromCriteria = ""
  DTMF_INPUT = ""
  nMainMenuLoopCnt = ""
  StatusOption = ""
  szMenuWav = ""
  nDiffTime = ""
  iPinRetries = ""
  mGetAllVoicemails2 = ""
  DTMF_MMENU = ""
  bUsePINPrompt = ""
  szMailCallerName = ""
  szPassword = ""
  szInput = ""
  mGetAllVoicemailsAll = ""
  szForwardExtension = ""
  mGetNewVoicemails = ""
  writeTime = ""
  dateEntry = ""
  szMailTime = ""
  bPINok = ""
  writeDate = ""
  szFrom = ""
  bEnabled = ""
  bIsVoicemail = ""
  szMailFolder = ""
  nMailLength = ""
  szMailDate = ""
  szVMailWav = ""
  
  PBXScriptOutputTrace "Input parameters:"
  'extract input parameters
  bEnabled = rInputParams.GetParamValueByName("bEnabled", True)
  PBXScriptOutputTrace "bEnabled = " & bEnabled
  szServerName = rInputParams.GetParamValueByName("szServerName", "")
  PBXScriptOutputTrace "szServerName = " & szServerName
  szUserName = rInputParams.GetParamValueByName("szUserName", "")
  PBXScriptOutputTrace "szUserName = " & szUserName
  szPassword = rInputParams.GetParamValueByName("szPassword", "")
  PBXScriptOutputTrace "szPassword = " & szPassword
  szMailFolder = rInputParams.GetParamValueByName("szMailFolder", "INBOX")
  PBXScriptOutputTrace "szMailFolder = " & szMailFolder
  bUseSSL = rInputParams.GetParamValueByName("bUseSSL", False)
  PBXScriptOutputTrace "bUseSSL = " & bUseSSL
  bUsePINPrompt = rInputParams.GetParamValueByName("bUsePINPrompt", True)
  PBXScriptOutputTrace "bUsePINPrompt = " & bUsePINPrompt
  szPIN = rInputParams.GetParamValueByName("szPIN", "")
  PBXScriptOutputTrace "szPIN = " & szPIN
  bUseFromCriteria = rInputParams.GetParamValueByName("bUseFromCriteria", True)
  PBXScriptOutputTrace "bUseFromCriteria = " & bUseFromCriteria
  
  Dim gseIMAP4Var_1(16)
  
  gseIMAP4_Initialize gseIMAP4Var_1
  
  Do
    Select Case szState
    
      '////////////////////////
      Case "Start0"
        PBXScriptOutputTrace "  case [Start0]"
        retVal = gseStart()
        Select Case retVal
          Case gseStateStarted
            szState = "SetVariable16"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "End1"
        PBXScriptOutputTrace "  case [End1]"
        rInputParams.m_FunctionResult = fnResults
        Exit Do
      
      '////////////////////////
      Case "Disconnect2"
        PBXScriptOutputTrace "  case [Disconnect2]"
        If (Not bWasDisconnect) Then
          szState = "SetVariable71"
          bWasDisconnect = True
        Else
          PBXScriptOutputTrace "GSE anti-LOOP script executed"
          Exit Do
        End If
      
      '////////////////////////
      Case "PlaySound3"
        PBXScriptOutputTrace "  case [PlaySound3]"
        retVal = gsePlaySoundEx2("Announcements/RI_no_new_voicem.wav", "0123456789*#", True, DTMF_INPUT, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable151"
          Case gseStateDTMFKeyPressed
            szState = "SetVariable151"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound4"
        PBXScriptOutputTrace "  case [PlaySound4]"
        retVal = gsePlaySoundEx2("Announcements/RI_wrong_param.wav", "", False, rcDummy, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable151"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate5"
        PBXScriptOutputTrace "  case [Evaluate5]"
        PBXScriptOutputTrace "nNewVoiceMailCount=0"
        retVal = gseEvaluateEx(nNewVoiceMailCount=0)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "Evaluate59"
          Case gseStateEvaluateNoMatch
            szState = "PlaySound50"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound6"
        PBXScriptOutputTrace "  case [PlaySound6]"
        retVal = gsePlaySoundEx2("Announcements/RI_new_voicemails.wav", "", False, rcDummy, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SayNumber7"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SayNumber7"
        PBXScriptOutputTrace "  case [SayNumber7]"
        retVal = gseSayNumberEx(nNewVoiceMailCount, "", False, rcDummy, True)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable133"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFChar8"
        PBXScriptOutputTrace "  case [GetDTMFChar8]"
        retVal = gseGetDTMFCharEx3("av_ri_mainMenu.wav", "0", "1", "3", True, DTMF_MMENU, True, True, DTMF_INPUT, "0123456789*#")
        Select Case retVal
          Case gseStateKey0
            szState = "End1"
          Case gseStateKey1
            szState = "SetVariable123"
          Case gseStateKey2
            szState = "GetDTMFChar170"
          Case gseStateKey3
            szState = "SetVariable107"
          Case gseStateKey4
            szState = "RunEditorAction70"
          Case gseStateKey5
            szState = "SetVariable87"
          Case gseStateKey6
            szState = "SetVariable77"
          Case gseStateKey7
            szState = "GetDTMFChar47"
          Case gseStateKey8
            szState = "PlaySound78"
          Case gseStateKey9
            szState = "PlaySound39"
          Case gseStateKey10
            szState = "PlaySound78"
          Case gseStateKey11
            szState = "PlaySound39"
          Case gseStateTimeout
            szState = "SetVariable149"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound9"
        PBXScriptOutputTrace "  case [PlaySound9]"
        retVal = gsePlaySoundEx2("Announcements/RI_received_on.wav", "0123456789*#", True, DTMF_INPUT, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SayDate64"
          Case gseStateDTMFKeyPressed
            szState = "Evaluate41"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound10"
        PBXScriptOutputTrace "  case [PlaySound10]"
        retVal = gsePlaySoundEx2("Announcements/RI_CallerID.wav", "0123456789*#", True, DTMF_INPUT, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SayNumber12"
          Case gseStateDTMFKeyPressed
            szState = "Evaluate43"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate11"
        PBXScriptOutputTrace "  case [Evaluate11]"
        PBXScriptOutputTrace "szMailCallerID<>"""""
        retVal = gseEvaluateEx(szMailCallerID<>"")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "SetVariable65"
          Case gseStateEvaluateNoMatch
            szState = "SetVariable66"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SayNumber12"
        PBXScriptOutputTrace "  case [SayNumber12]"
        retVal = gseSayNumberEx(szMailCallerID, "0123456789*#", True, DTMF_INPUT, True)
        Select Case retVal
          Case gseStatePlayed
            szState = "Evaluate35"
          Case gseStateDTMFKeyPressed
            szState = "Evaluate44"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound13"
        PBXScriptOutputTrace "  case [PlaySound13]"
        retVal = gsePlaySoundEx2("Announcements/RI_welcome.wav", "0123456789*#", True, szInput, False, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable33"
          Case gseStateDTMFKeyPressed
            szState = "GetDTMFString56"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_ConnectToServer14"
        PBXScriptOutputTrace "  case [IMAP4_ConnectToServer14]"
        retVal = gseIMAP4_ConnectToServerEx3(gseIMAP4Var_1, szServerName, szUserName, szPassword, False, szMailFolder, False, False, rcDummy, False, rcDummy)
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "Evaluate138"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_SeekMail15"
        PBXScriptOutputTrace "  case [IMAP4_SeekMail15]"
        retVal = gseIMAP4_SeekMailEx(gseIMAP4Var_1, "0")
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "IMAP4_GetMailAtributes60"
          Case gseStateIMAP4Failed
            szState = "PlaySound32"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable16"
        PBXScriptOutputTrace "  case [SetVariable16]"
        PBXcall.alerting
        PBXScriptOutputTrace "exp: " & "PBXcall.alerting"
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "nNewVoiceMailCount = 0"
        PBXScriptOutputTrace "res: nNewVoiceMailCount = " & rcDummy
        nNewVoiceMailCount = rcDummy
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "nOldVoiceMailCount = 0"
        PBXScriptOutputTrace "res: nOldVoiceMailCount = " & rcDummy
        nOldVoiceMailCount = rcDummy
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "bIsVoicemail = 0"
        PBXScriptOutputTrace "res: bIsVoicemail = " & rcDummy
        bIsVoicemail = rcDummy
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "nMailLength = 0"
        PBXScriptOutputTrace "res: nMailLength = " & rcDummy
        nMailLength = rcDummy
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "bPINok = 0"
        PBXScriptOutputTrace "res: bPINok = " & rcDummy
        bPINok = rcDummy
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "szInput = """""
        PBXScriptOutputTrace "res: szInput = " & rcDummy
        szInput = rcDummy
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "szMenuWav = """""
        PBXScriptOutputTrace "res: szMenuWav = " & rcDummy
        szMenuWav = rcDummy
        rcDummy = gseStateIMAP4Success
        PBXScriptOutputTrace "exp: " & "fnResults = gseStateIMAP4Success"
        PBXScriptOutputTrace "res: fnResults = " & rcDummy
        fnResults = rcDummy
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "nDiffTime = """""
        PBXScriptOutputTrace "res: nDiffTime = " & rcDummy
        nDiffTime = rcDummy
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "nStartTime = """""
        PBXScriptOutputTrace "res: nStartTime = " & rcDummy
        nStartTime = rcDummy
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "nStartDateTime = """""
        PBXScriptOutputTrace "res: nStartDateTime = " & rcDummy
        nStartDateTime = rcDummy
        rcDummy = SelectMainMenuSound()
        PBXScriptOutputTrace "exp: " & "szMainMenuSound = SelectMainMenuSound()"
        PBXScriptOutputTrace "res: szMainMenuSound = " & rcDummy
        szMainMenuSound = rcDummy
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "nMainMenuLoopCnt = 0"
        PBXScriptOutputTrace "res: nMainMenuLoopCnt = " & rcDummy
        nMainMenuLoopCnt = rcDummy
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "writeTime = """""
        PBXScriptOutputTrace "res: writeTime = " & rcDummy
        writeTime = rcDummy
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "writeDate = """""
        PBXScriptOutputTrace "res: writeDate = " & rcDummy
        writeDate = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate72"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_GetMails17"
        PBXScriptOutputTrace "  case [IMAP4_GetMails17]"
        retVal = gseIMAP4_GetMailsEx4(gseIMAP4Var_1, True, True, True, False, nNewVoiceMailCount, "", "", OnlyFromOriginator(bUseFromCriteria), "True")
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "Evaluate130"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateIMAP4FetchCmdRunning
            szState = "gseWaitForDisconnect166"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_SaveVoicemailToTmpFile18"
        PBXScriptOutputTrace "  case [IMAP4_SaveVoicemailToTmpFile18]"
        retVal = gseIMAP4_SaveVoicemailToTmpFileEx3(gseIMAP4Var_1, True, szVMailWav)
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "PlaySound38"
          Case gseStateIMAP4Failed
            szState = "GetDTMFChar29"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SayTime19"
        PBXScriptOutputTrace "  case [SayTime19]"
        retVal = gseSayTimeEx(szMailTime, False, "0123456789*#", True, DTMF_INPUT, True)
        Select Case retVal
          Case gseStatePlayed
            szState = "Evaluate11"
          Case gseStateDTMFKeyPressed
            szState = "Evaluate45"
          Case gseStateFailed
            szState = "Evaluate11"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_SeekMail20"
        PBXScriptOutputTrace "  case [IMAP4_SeekMail20]"
        retVal = gseIMAP4_SeekMailEx(gseIMAP4Var_1, "2")
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "IMAP4_GetMailAtributes60"
          Case gseStateIMAP4Failed
            szState = "SetVariable151"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_DeleteMail21"
        PBXScriptOutputTrace "  case [IMAP4_DeleteMail21]"
        retVal = gseIMAP4_DeleteMailEx(gseIMAP4Var_1)
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "PlaySound28"
          Case gseStateIMAP4Failed
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFString22"
        PBXScriptOutputTrace "  case [GetDTMFString22]"
        retVal = gseGetDTMFStringEx2(szInput, False, "Announcements/RI_enter_PIN.wav", True, False, "#", "2", "10")
        Select Case retVal
          Case gseStateDTMFStringCollected
            szState = "Evaluate23"
          Case gseStateTimeout
            szState = "SetVariable33"
          Case gseStateOnlyStopChar
            szState = "SetVariable33"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate23"
        PBXScriptOutputTrace "  case [Evaluate23]"
        PBXScriptOutputTrace "PBXScript.CheckPIN(szInput, szPIN) = PBXCheckPinResultOk"
        retVal = gseEvaluateEx(PBXScript.CheckPIN(szInput, szPIN) = PBXCheckPinResultOk)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "PlaySound26"
          Case gseStateEvaluateNoMatch
            szState = "PlaySound24"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound24"
        PBXScriptOutputTrace "  case [PlaySound24]"
        retVal = gsePlaySoundEx2("Announcements/RI_PIN_invalid.wav", "0123456789*#", True, szInput, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable33"
          Case gseStateDTMFKeyPressed
            szState = "SetVariable147"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate25"
        PBXScriptOutputTrace "  case [Evaluate25]"
        PBXScriptOutputTrace "Extension()=CallerID()"
        retVal = gseEvaluateEx(Extension()=CallerID())
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "InsertScript76"
          Case gseStateEvaluateNoMatch
            szState = "SetVariable145"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound26"
        PBXScriptOutputTrace "  case [PlaySound26]"
        retVal = gsePlaySoundEx2("beep.wav", "", False, rcDummy, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable58"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFChar27"
        PBXScriptOutputTrace "  case [GetDTMFChar27]"
        retVal = gseGetDTMFCharEx3("Announcements/RI_confirm.wav", "0", "1", "4", False, rcDummy, True, False, rcDummy, "0123456789*#")
        Select Case retVal
          Case gseStateKey0
            szState = "IMAP4_SeekMail20"
          Case gseStateKey1
            szState = "IMAP4_SeekMail20"
          Case gseStateKey2
            szState = "IMAP4_SeekMail20"
          Case gseStateKey3
            szState = "IMAP4_SeekMail20"
          Case gseStateKey4
            szState = "IMAP4_SeekMail20"
          Case gseStateKey5
            szState = "IMAP4_SeekMail20"
          Case gseStateKey6
            szState = "IMAP4_SeekMail20"
          Case gseStateKey7
            szState = "IMAP4_SeekMail20"
          Case gseStateKey8
            szState = "IMAP4_SeekMail20"
          Case gseStateKey9
            szState = "IMAP4_SeekMail20"
          Case gseStateKey10
            szState = "IMAP4_IsLastMail52"
          Case gseStateKey11
            szState = "IMAP4_SeekMail20"
          Case gseStateTimeout
            szState = "IMAP4_SeekMail20"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound28"
        PBXScriptOutputTrace "  case [PlaySound28]"
        retVal = gsePlaySoundEx2("Announcements/RI_deleted.wav", "", False, rcDummy, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "Evaluate55"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFChar29"
        PBXScriptOutputTrace "  case [GetDTMFChar29]"
        retVal = gseGetDTMFCharEx3(szMenuWav, "0", "1", "2", True, DTMF_SMENU, True, True, DTMF_INPUT, "0123456789*#")
        Select Case retVal
          Case gseStateKey0
            szState = "Evaluate67"
          Case gseStateKey1
            szState = "PlaySound9"
          Case gseStateKey2
            szState = "PlaySound79"
          Case gseStateKey3
            szState = "GetDTMFChar27"
          Case gseStateKey4
            szState = "SetVariable89"
          Case gseStateKey5
            szState = "PlaySound79"
          Case gseStateKey6
            szState = "Evaluate104"
          Case gseStateKey7
            szState = "IMAP4_SeekMail61"
          Case gseStateKey8
            szState = "IMAP4_SeekMail20"
          Case gseStateKey9
            szState = "Evaluate138"
          Case gseStateKey10
            szState = "IMAP4_SeekMail20"
          Case gseStateKey11
            szState = "GetDTMFChar29"
          Case gseStateTimeout
            szState = "SetVariable121"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "ConnectTo30"
        PBXScriptOutputTrace "  case [ConnectTo30]"
        retVal = gseConnectToEx6(szMailCallerID, "30", "", False, rcDummy, True, False, "", False, "", True)
        Select Case retVal
          Case gseStateConnected
            szState = "End1"
          Case gseStateTimeout
            szState = "PlaySound40"
          Case gseStateNoAnswer
            szState = "PlaySound40"
          Case gseStateNotDelivered
            szState = "PlaySound40"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateProceedWDstScr
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_GetMails31"
        PBXScriptOutputTrace "  case [IMAP4_GetMails31]"
        retVal = gseIMAP4_GetMailsEx4(gseIMAP4Var_1, False, False, False, False, nNewVoiceMailCount, "", "", "", "True")
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "InsertScript109"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateIMAP4FetchCmdRunning
            szState = "gseWaitForDisconnect167"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound32"
        PBXScriptOutputTrace "  case [PlaySound32]"
        retVal = gsePlaySoundEx2("Announcements/RI_no_voicemail.wav", "0123456789*#", True, DTMF_INPUT, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable151"
          Case gseStateDTMFKeyPressed
            szState = "SetVariable151"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable33"
        PBXScriptOutputTrace "  case [SetVariable33]"
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "szInput = """""
        PBXScriptOutputTrace "res: szInput = " & rcDummy
        szInput = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "SetVariable147"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate34"
        PBXScriptOutputTrace "  case [Evaluate34]"
        PBXScriptOutputTrace "bIsVoicemail=1"
        retVal = gseEvaluateEx(bIsVoicemail=1)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "IMAP4_SaveVoicemailToTmpFile18"
          Case gseStateEvaluateNoMatch
            szState = "GetDTMFChar29"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate35"
        PBXScriptOutputTrace "  case [Evaluate35]"
        PBXScriptOutputTrace "szMailCallerName="""""
        retVal = gseEvaluateEx(szMailCallerName="")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "Evaluate34"
          Case gseStateEvaluateNoMatch
            szState = "SetVariable36"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable36"
        PBXScriptOutputTrace "  case [SetVariable36]"
        rcDummy = gseSayName(szMailCallerName)
        PBXScriptOutputTrace "exp: " & "szInput = gseSayName(szMailCallerName)"
        PBXScriptOutputTrace "res: szInput = " & rcDummy
        szInput = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate34"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_SetMailAtributes37"
        PBXScriptOutputTrace "  case [IMAP4_SetMailAtributes37]"
        retVal = gseIMAP4_SetMailAtributesEx2(gseIMAP4Var_1, True, "0")
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "GetDTMFChar29"
          Case gseStateIMAP4Failed
            szState = "GetDTMFChar29"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound38"
        PBXScriptOutputTrace "  case [PlaySound38]"
        retVal = gsePlaySoundEx2(szVMailWav, "*", True, DTMF_INPUT, True, False, "1", True)
        Select Case retVal
          Case gseStatePlayed
            szState = "IMAP4_SetMailAtributes37"
          Case gseStateDTMFKeyPressed
            szState = "GetDTMFChar29"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound39"
        PBXScriptOutputTrace "  case [PlaySound39]"
        retVal = gsePlaySoundEx2("Announcements/RI_help.wav", "0123456789*#", True, DTMF_INPUT, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable81"
          Case gseStateDTMFKeyPressed
            szState = "SetVariable81"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound40"
        PBXScriptOutputTrace "  case [PlaySound40]"
        retVal = gsePlaySoundEx2("Announcements/RI_Subscriber_not_reach.wav", "", False, rcDummy, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "IMAP4_SeekMail20"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate41"
        PBXScriptOutputTrace "  case [Evaluate41]"
        PBXScriptOutputTrace "DTMF_INPUT=""*"""
        retVal = gseEvaluateEx(DTMF_INPUT="*")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "SetVariable62"
          Case gseStateEvaluateNoMatch
            szState = "GetDTMFChar29"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate42"
        PBXScriptOutputTrace "  case [Evaluate42]"
        PBXScriptOutputTrace "DTMF_INPUT=""*"""
        retVal = gseEvaluateEx(DTMF_INPUT="*")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "SetVariable62"
          Case gseStateEvaluateNoMatch
            szState = "GetDTMFChar29"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate43"
        PBXScriptOutputTrace "  case [Evaluate43]"
        PBXScriptOutputTrace "DTMF_INPUT=""*"""
        retVal = gseEvaluateEx(DTMF_INPUT="*")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "SetVariable63"
          Case gseStateEvaluateNoMatch
            szState = "GetDTMFChar29"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate44"
        PBXScriptOutputTrace "  case [Evaluate44]"
        PBXScriptOutputTrace "DTMF_INPUT=""*"""
        retVal = gseEvaluateEx(DTMF_INPUT="*")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "SetVariable63"
          Case gseStateEvaluateNoMatch
            szState = "GetDTMFChar29"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate45"
        PBXScriptOutputTrace "  case [Evaluate45]"
        PBXScriptOutputTrace "DTMF_INPUT=""*"""
        retVal = gseEvaluateEx(DTMF_INPUT="*")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "SetVariable62"
          Case gseStateEvaluateNoMatch
            szState = "GetDTMFChar29"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_GetMails46"
        PBXScriptOutputTrace "  case [IMAP4_GetMails46]"
        retVal = gseIMAP4_GetMailsEx4(gseIMAP4Var_1, True, True, False, False, nOldVoiceMailCount, "", "", OnlyFromOriginator(bUseFromCriteria), "True")
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "InsertScript117"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateIMAP4FetchCmdRunning
            szState = "gseWaitForDisconnect169"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFChar47"
        PBXScriptOutputTrace "  case [GetDTMFChar47]"
        retVal = gseGetDTMFCharEx3("Announcements/RI_confirm_all.wav", "0", "1", "10", False, rcDummy, True, False, rcDummy, "0123456789*#")
        Select Case retVal
          Case gseStateKey0
            szState = "SetVariable151"
          Case gseStateKey1
            szState = "SetVariable151"
          Case gseStateKey2
            szState = "SetVariable151"
          Case gseStateKey3
            szState = "SetVariable151"
          Case gseStateKey4
            szState = "SetVariable151"
          Case gseStateKey5
            szState = "SetVariable151"
          Case gseStateKey6
            szState = "SetVariable151"
          Case gseStateKey7
            szState = "SetVariable151"
          Case gseStateKey8
            szState = "SetVariable151"
          Case gseStateKey9
            szState = "SetVariable151"
          Case gseStateKey10
            szState = "SetVariable116"
          Case gseStateKey11
            szState = "SetVariable151"
          Case gseStateTimeout
            szState = "SetVariable151"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_SeekMail48"
        PBXScriptOutputTrace "  case [IMAP4_SeekMail48]"
        retVal = gseIMAP4_SeekMailEx(gseIMAP4Var_1, "0")
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "IMAP4_DeleteMail49"
          Case gseStateIMAP4Failed
            szState = "PlaySound51"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_DeleteMail49"
        PBXScriptOutputTrace "  case [IMAP4_DeleteMail49]"
        retVal = gseIMAP4_DeleteMailEx(gseIMAP4Var_1)
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "IMAP4_SeekMail48"
          Case gseStateIMAP4Failed
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound50"
        PBXScriptOutputTrace "  case [PlaySound50]"
        retVal = gsePlaySoundEx2("Announcements/1_second_silence.wav", "", False, rcDummy, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "PlaySound6"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound51"
        PBXScriptOutputTrace "  case [PlaySound51]"
        retVal = gsePlaySoundEx2("Announcements/RI_deleted_all.wav", "", False, rcDummy, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable151"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_IsLastMail52"
        PBXScriptOutputTrace "  case [IMAP4_IsLastMail52]"
        retVal = gseIMAP4_IsLastMailEx(gseIMAP4Var_1)
        Select Case retVal
          Case gseStateIMAP4True
            szState = "SetVariable53"
          Case gseStateIMAP4False
            szState = "SetVariable54"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable53"
        PBXScriptOutputTrace "  case [SetVariable53]"
        rcDummy = True
        PBXScriptOutputTrace "exp: " & "bLast = True"
        PBXScriptOutputTrace "res: bLast = " & rcDummy
        bLast = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "IMAP4_DeleteMail21"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable54"
        PBXScriptOutputTrace "  case [SetVariable54]"
        rcDummy = False
        PBXScriptOutputTrace "exp: " & "bLast = False"
        PBXScriptOutputTrace "res: bLast = " & rcDummy
        bLast = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "IMAP4_DeleteMail21"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate55"
        PBXScriptOutputTrace "  case [Evaluate55]"
        PBXScriptOutputTrace "bLast=True"
        retVal = gseEvaluateEx(bLast=True)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "SetVariable151"
          Case gseStateEvaluateNoMatch
            szState = "IMAP4_GetMailAtributes60"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFString56"
        PBXScriptOutputTrace "  case [GetDTMFString56]"
        retVal = gseGetDTMFStringEx2(szInput, False, "", True, False, "#", "30", "10")
        Select Case retVal
          Case gseStateDTMFStringCollected
            szState = "Evaluate23"
          Case gseStateTimeout
            szState = "SetVariable33"
          Case gseStateOnlyStopChar
            szState = "SetVariable33"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable57"
        PBXScriptOutputTrace "  case [SetVariable57]"
        PBXscript.sleep(12000)
        PBXScriptOutputTrace "exp: " & "PBXscript.sleep(12000)"
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "PlaySound3"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable58"
        PBXScriptOutputTrace "  case [SetVariable58]"
        rcDummy = 1
        PBXScriptOutputTrace "exp: " & "bPINok = 1"
        PBXScriptOutputTrace "res: bPINok = " & rcDummy
        bPINok = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "InsertScript76"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate59"
        PBXScriptOutputTrace "  case [Evaluate59]"
        PBXScriptOutputTrace "bPINok=1"
        retVal = gseEvaluateEx(bPINok=1)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "PlaySound3"
          Case gseStateEvaluateNoMatch
            szState = "Call68"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_GetMailAtributes60"
        PBXScriptOutputTrace "  case [IMAP4_GetMailAtributes60]"
        retVal = gseIMAP4_GetMailAtributesEx3(gseIMAP4Var_1, True, szMailDate, True, szMailTime, True, szMailCallerID, True, szMailCallerName, True, bIsVoicemail, True, nMailLength, False, rcDummy, False, rcDummy, False, rcDummy)
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "IMAP4_GetMail152"
          Case gseStateIMAP4Failed
            szState = "PlaySound32"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_SeekMail61"
        PBXScriptOutputTrace "  case [IMAP4_SeekMail61]"
        retVal = gseIMAP4_SeekMailEx(gseIMAP4Var_1, "1")
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "IMAP4_GetMailAtributes60"
          Case gseStateIMAP4Failed
            szState = "SetVariable151"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable62"
        PBXScriptOutputTrace "  case [SetVariable62]"
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "DTMF_INPUT = """""
        PBXScriptOutputTrace "res: DTMF_INPUT = " & rcDummy
        DTMF_INPUT = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate11"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable63"
        PBXScriptOutputTrace "  case [SetVariable63]"
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "DTMF_INPUT = """""
        PBXScriptOutputTrace "res: DTMF_INPUT = " & rcDummy
        DTMF_INPUT = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate35"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SayDate64"
        PBXScriptOutputTrace "  case [SayDate64]"
        retVal = gseSayDateEx2(szMailDate, True, "0123456789*#", True, DTMF_INPUT, True, True)
        Select Case retVal
          Case gseStatePlayed
            szState = "SayTime19"
          Case gseStateDTMFKeyPressed
            szState = "Evaluate42"
          Case gseStateFailed
            szState = "SayTime19"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable65"
        PBXScriptOutputTrace "  case [SetVariable65]"
        rcDummy = "Announcements/RI_delete_connect_menu.wav"
        PBXScriptOutputTrace "exp: " & "szMenuWav = ""Announcements/RI_delete_connect_menu.wav"""
        PBXScriptOutputTrace "res: szMenuWav = " & rcDummy
        szMenuWav = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "PlaySound10"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable66"
        PBXScriptOutputTrace "  case [SetVariable66]"
        rcDummy = "Announcements/RI_delete_menu.wav"
        PBXScriptOutputTrace "exp: " & "szMenuWav = ""Announcements/RI_delete_menu.wav"""
        PBXScriptOutputTrace "res: szMenuWav = " & rcDummy
        szMenuWav = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate34"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate67"
        PBXScriptOutputTrace "  case [Evaluate67]"
        PBXScriptOutputTrace "szMailCallerID<>"""""
        retVal = gseEvaluateEx(szMailCallerID<>"")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "ConnectTo30"
          Case gseStateEvaluateNoMatch
            szState = "GetDTMFChar29"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Call68"
        PBXScriptOutputTrace "  case [Call68]"
        retVal = gseCalls((InternalCall()))
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateTrue
            szState = "Evaluate82"
          Case gseStateFalse
            szState = "SetVariable57"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate69"
        PBXScriptOutputTrace "  case [Evaluate69]"
        PBXScriptOutputTrace "bIsVoicemail=1"
        retVal = gseEvaluateEx(bIsVoicemail=1)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "PlaySound9"
          Case gseStateEvaluateNoMatch
            szState = "SetVariable151"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "RunEditorAction70"
        PBXScriptOutputTrace "  case [RunEditorAction70]"
        Dim rInParams_71_actionRedirection
        Set rInParams_71_actionRedirection = new GSEParamList
        rInParams_71_actionRedirection.Count = 1
        rInParams_71_actionRedirection.AddParam "DTMF_INPUT", DTMF_INPUT
        
        actionRedirection rInParams_71_actionRedirection
        
        DTMF_INPUT = rInParams_71_actionRedirection.GetParamValueByName("DTMF_INPUT", DTMF_INPUT)
        Select Case CStr(rInParams_71_actionRedirection.m_FunctionResult)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateExecuted
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateExecuted
            szState = "SetVariable151"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable71"
        PBXScriptOutputTrace "  case [SetVariable71]"
        rcDummy = gseStateDisconnected
        PBXScriptOutputTrace "exp: " & "fnResults = gseStateDisconnected"
        PBXScriptOutputTrace "res: fnResults = " & rcDummy
        fnResults = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "End1"
          Case gseStateDisconnected
            szState = "End1"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate72"
        PBXScriptOutputTrace "  case [Evaluate72]"
        PBXScriptOutputTrace "bUsePINPrompt"
        retVal = gseEvaluateEx(bUsePINPrompt)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "Evaluate73"
          Case gseStateEvaluateNoMatch
            szState = "InsertScript76"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate73"
        PBXScriptOutputTrace "  case [Evaluate73]"
        PBXScriptOutputTrace "bEnabled = ""Voicemail.True"""
        retVal = gseEvaluateEx(bEnabled = "Voicemail.True")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "Evaluate25"
          Case gseStateEvaluateNoMatch
            szState = "SetVariable145"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_ConnectToServer74"
        PBXScriptOutputTrace "  case [IMAP4_ConnectToServer74]"
        retVal = gseIMAP4_ConnectToServerEx3(gseIMAP4Var_1, szServerName, szUserName, szPassword, False, szMailFolder, True, False, rcDummy, False, rcDummy)
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "Evaluate138"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate75"
        PBXScriptOutputTrace "  case [Evaluate75]"
        PBXScriptOutputTrace "bUseSSL=True"
        retVal = gseEvaluateEx(bUseSSL=True)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "IMAP4_ConnectToServer74"
          Case gseStateEvaluateNoMatch
            szState = "IMAP4_ConnectToServer14"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript76"
        PBXScriptOutputTrace "  case [InsertScript76]"
        
        UseExit = 0 ' default exit
        
        PBXCall.PhoneCallListEntry.Delete
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "Evaluate75"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable77"
        PBXScriptOutputTrace "  case [SetVariable77]"
        rcDummy = "6"
        PBXScriptOutputTrace "exp: " & "DTMF_INPUT = ""6"""
        PBXScriptOutputTrace "res: DTMF_INPUT = " & rcDummy
        DTMF_INPUT = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "RunEditorAction70"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound78"
        PBXScriptOutputTrace "  case [PlaySound78]"
        retVal = gsePlaySoundEx2("Announcements/RI_not_implemented.wav", "0123456789*#", True, DTMF_INPUT, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable81"
          Case gseStateDTMFKeyPressed
            szState = "SetVariable81"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound79"
        PBXScriptOutputTrace "  case [PlaySound79]"
        retVal = gsePlaySoundEx2("Announcements/RI_not_implemented.wav", "0123456789*#", True, DTMF_INPUT, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable80"
          Case gseStateDTMFKeyPressed
            szState = "SetVariable80"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable80"
        PBXScriptOutputTrace "  case [SetVariable80]"
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "DTMF_INPUT = """""
        PBXScriptOutputTrace "res: DTMF_INPUT = " & rcDummy
        DTMF_INPUT = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "GetDTMFChar29"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable81"
        PBXScriptOutputTrace "  case [SetVariable81]"
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "DTMF_INPUT = """""
        PBXScriptOutputTrace "res: DTMF_INPUT = " & rcDummy
        DTMF_INPUT = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "SetVariable151"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate82"
        PBXScriptOutputTrace "  case [Evaluate82]"
        PBXScriptOutputTrace "PostDialingDigits() <> """""
        retVal = gseEvaluateEx(PostDialingDigits() <> "")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "SetVariable83"
          Case gseStateEvaluateNoMatch
            szState = "PlaySound3"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable83"
        PBXScriptOutputTrace "  case [SetVariable83]"
        rcDummy = PostDialingDigits()
        PBXScriptOutputTrace "exp: " & "DTMF_INPUT = PostDialingDigits()"
        PBXScriptOutputTrace "res: DTMF_INPUT = " & rcDummy
        DTMF_INPUT = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "SetVariable151"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SendEmail84"
        PBXScriptOutputTrace "  case [SendEmail84]"
        retVal = gseSendEMailEx3(szUserEmailAddress, g_resStandardNoticeSubject, g_resStandardNoticeBody, "Last", "", "", "", "", "", False, "rm")
        Select Case retVal
          Case gseStateSent
            szState = "PlaySound102"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "RecordMessage85"
        PBXScriptOutputTrace "  case [RecordMessage85]"
        retVal = gseRecordMessageEx4("600", True, False, rcDummy, "#", False, rcDummy, True, True, "5")
        Select Case retVal
          Case gseStateTimeout
            szState = "SendEmail84"
          Case gseStateRecorded
            szState = "SendEmail84"
          Case gseStateDTMFKeyPressed
            szState = "SendEmail84"
          Case gseStateSilenceDetected
            szState = "SendEmail84"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound86"
        PBXScriptOutputTrace "  case [PlaySound86]"
        retVal = gsePlaySoundEx2("Announcements/RI_Record_Notice.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "RecordMessage85"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable87"
        PBXScriptOutputTrace "  case [SetVariable87]"
        rcDummy = PBXUser.VoicemailEMailAddress
        PBXScriptOutputTrace "exp: " & "szUserEmailAddress = PBXUser.VoicemailEMailAddress"
        PBXScriptOutputTrace "res: szUserEmailAddress = " & rcDummy
        szUserEmailAddress = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate93"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound88"
        PBXScriptOutputTrace "  case [PlaySound88]"
        retVal = gsePlaySoundEx2("Announcements/RI_Record_Reply.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "RecordMessage100"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable89"
        PBXScriptOutputTrace "  case [SetVariable89]"
        rcDummy = GetEmailAddrFromCallerID(szMailCallerID, szFromFullStr)
        PBXScriptOutputTrace "exp: " & "szUserEmailAddress = GetEmailAddrFromCallerID(szMailCallerID, szFromFullStr)"
        PBXScriptOutputTrace "res: szUserEmailAddress = " & rcDummy
        szUserEmailAddress = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate90"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate90"
        PBXScriptOutputTrace "  case [Evaluate90]"
        PBXScriptOutputTrace "szUserEmailAddress<>"""""
        retVal = gseEvaluateEx(szUserEmailAddress<>"")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "PlaySound88"
          Case gseStateEvaluateNoMatch
            szState = "PlaySound91"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound91"
        PBXScriptOutputTrace "  case [PlaySound91]"
        retVal = gsePlaySoundEx2("Announcements/RI_No_Reply_possible.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable80"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound92"
        PBXScriptOutputTrace "  case [PlaySound92]"
        retVal = gsePlaySoundEx2("Announcements/RI_No_Notice_possible.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable80"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate93"
        PBXScriptOutputTrace "  case [Evaluate93]"
        PBXScriptOutputTrace "szUserEmailAddress<>"""""
        retVal = gseEvaluateEx(szUserEmailAddress<>"")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "PlaySound86"
          Case gseStateEvaluateNoMatch
            szState = "PlaySound92"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound94"
        PBXScriptOutputTrace "  case [PlaySound94]"
        retVal = gsePlaySoundEx2("Announcements/RI_Forward_OK.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable105"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable95"
        PBXScriptOutputTrace "  case [SetVariable95]"
        rcDummy = UserEmailAddress(szForwardExtension)
        PBXScriptOutputTrace "exp: " & "szUserEmailAddress = UserEmailAddress(szForwardExtension)"
        PBXScriptOutputTrace "res: szUserEmailAddress = " & rcDummy
        szUserEmailAddress = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate96"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate96"
        PBXScriptOutputTrace "  case [Evaluate96]"
        PBXScriptOutputTrace "szUserEmailAddress<>"""""
        retVal = gseEvaluateEx(szUserEmailAddress<>"")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "InsertScript103"
          Case gseStateEvaluateNoMatch
            szState = "PlaySound97"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound97"
        PBXScriptOutputTrace "  case [PlaySound97]"
        retVal = gsePlaySoundEx2("Announcements/RI_No_Forward_possible.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable80"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFString98"
        PBXScriptOutputTrace "  case [GetDTMFString98]"
        retVal = gseGetDTMFStringEx2(szForwardExtension, True, "Announcements/RI_Get_Forward_Extension.wav", True, True, "#", "5", "5")
        Select Case retVal
          Case gseStateDTMFStringCollected
            szState = "SetVariable95"
          Case gseStateTimeout
            szState = "SetVariable80"
          Case gseStateOnlyStopChar
            szState = "SetVariable80"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SendEmail99"
        PBXScriptOutputTrace "  case [SendEmail99]"
        retVal = gseSendEMailEx3(szUserEmailAddress, g_resStandardReplySubject, g_resStandardReplyBody, "Last", "", "", "", "", "", False, "rm")
        Select Case retVal
          Case gseStateSent
            szState = "PlaySound101"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "RecordMessage100"
        PBXScriptOutputTrace "  case [RecordMessage100]"
        retVal = gseRecordMessageEx4("600", True, False, rcDummy, "#", False, rcDummy, True, True, "5")
        Select Case retVal
          Case gseStateTimeout
            szState = "SendEmail99"
          Case gseStateRecorded
            szState = "SendEmail99"
          Case gseStateDTMFKeyPressed
            szState = "SendEmail99"
          Case gseStateSilenceDetected
            szState = "SendEmail99"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound101"
        PBXScriptOutputTrace "  case [PlaySound101]"
        retVal = gsePlaySoundEx2("Announcements/RI_Reply_OK.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable80"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound102"
        PBXScriptOutputTrace "  case [PlaySound102]"
        retVal = gsePlaySoundEx2("Announcements/RI_Notice_OK.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable151"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript103"
        PBXScriptOutputTrace "  case [InsertScript103]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Geben Sie hier Ihren Skript-Code ein
        
        UseExit = 0 ' Bitte verwenden Sie die Variable UseExit um die Block-Ausgänge 0..9 zu verwenden
        
        retVal = PBXScript.SendEMail(PBXUser.VoicemailEMailAddress, PBXUser.VoicemailEMailAddress, _
                   szUserEmailAddress, "", "", _
                   g_resStandardForwardSubject, g_resStandardForwardBody, _
                   szVMailWav, "")
        
        ' UserEmailAddress(szMailCallerID), UserEmailAddress(szMailCallerID), 
        
          If retVal = PBXSuccess Then
            PBXScriptOutputTrace "PBXScript.SendEMail = PBXSuccess"
            
            Dim oUsersForNVM, oUserToSignal, nNewAndUnreadVMs
            oUsersForNVM = Empty
        
            If (Len(szUserEmailAddress) > 0) Then
              Set oUsersForNVM = g_PBXConfig.GetUserByAddress(szForwardExtension)
            End If
            
            PBXScriptOutputTrace "szForwardExtension=["&szForwardExtension&"], IsEmpty(oUsersForNVM)="&IsEmpty(oUsersForNVM)
        
            If Not IsEmpty(oUsersForNVM) Then
              'get number of new and unread VMs and increment by one
              For Each oUserToSignal In oUsersForNVM
                nNewAndUnreadVMs = oUserToSignal.NumberOfNewVoicemails + 1
                PBXScriptOutputTrace "Number of new and unread voicemails for additional user '" & oUserToSignal.Name & "' is " & nNewAndUnreadVMs
                oUserToSignal.NumberOfNewVoicemails = nNewAndUnreadVMs
              Next
            End If
          Else
            PBXScriptOutputTrace "PBXScript.SendEMail = PBXFailed"
          End If
        
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "SetVariable106"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate104"
        PBXScriptOutputTrace "  case [Evaluate104]"
        PBXScriptOutputTrace "szVMailWav="""""
        retVal = gseEvaluateEx(szVMailWav="")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "PlaySound97"
          Case gseStateEvaluateNoMatch
            szState = "GetDTMFString98"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable105"
        PBXScriptOutputTrace "  case [SetVariable105]"
        rcDummy = gseSayName(szMailCallerName)
        PBXScriptOutputTrace "exp: " & "szInput = gseSayName(szMailCallerName)"
        PBXScriptOutputTrace "res: szInput = " & rcDummy
        szInput = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "SetVariable80"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable106"
        PBXScriptOutputTrace "  case [SetVariable106]"
        rcDummy = GetUserName(szForwardExtension)
        PBXScriptOutputTrace "exp: " & "szMailCallerName = GetUserName(szForwardExtension)"
        PBXScriptOutputTrace "res: szMailCallerName = " & rcDummy
        szMailCallerName = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "PlaySound94"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable107"
        PBXScriptOutputTrace "  case [SetVariable107]"
        rcDummy = Now
        PBXScriptOutputTrace "exp: " & "nStartDateTime = Now"
        PBXScriptOutputTrace "res: nStartDateTime = " & rcDummy
        nStartDateTime = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "gseHold108"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseHold108"
        PBXScriptOutputTrace "  case [gseHold108]"
        retVal = gseHoldEx("Announcements/RI_Wait_Email.wav")
        Select Case retVal
          Case gseStateOnHold
            szState = "SetVariable164"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript109"
        PBXScriptOutputTrace "  case [InsertScript109]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        
        nDiffTime = DateDiff("s", nStartDateTime, Now)
        
        If (nDiffTime = 3) Then PBXScript.Sleep 2000
        If (nDiffTime = 3) Then PBXScript.Sleep 1000
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "gseActivate110"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseActivate110"
        PBXScriptOutputTrace "  case [gseActivate110]"
        retVal = gseActivate()
        Select Case retVal
          Case gseStateActivated
            szState = "PlaySound111"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound111"
        PBXScriptOutputTrace "  case [PlaySound111]"
        retVal = gsePlaySoundEx2("beep.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "IMAP4_SeekMail15"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable112"
        PBXScriptOutputTrace "  case [SetVariable112]"
        rcDummy = Now
        PBXScriptOutputTrace "exp: " & "nStartDateTime = Now"
        PBXScriptOutputTrace "res: nStartDateTime = " & rcDummy
        nStartDateTime = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "gseHold114"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript113"
        PBXScriptOutputTrace "  case [InsertScript113]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        
        nDiffTime = DateDiff("s", nStartDateTime, Now)
        
        If (nDiffTime = 3) Then PBXScript.Sleep 2000
        If (nDiffTime = 3) Then PBXScript.Sleep 1000
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "gseActivate115"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseHold114"
        PBXScriptOutputTrace "  case [gseHold114]"
        retVal = gseHoldEx("Announcements/RI_Wait.wav")
        Select Case retVal
          Case gseStateOnHold
            szState = "SetVariable162"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseActivate115"
        PBXScriptOutputTrace "  case [gseActivate115]"
        retVal = gseActivate()
        Select Case retVal
          Case gseStateActivated
            szState = "PlaySound128"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable116"
        PBXScriptOutputTrace "  case [SetVariable116]"
        rcDummy = Now
        PBXScriptOutputTrace "exp: " & "nStartDateTime = Now"
        PBXScriptOutputTrace "res: nStartDateTime = " & rcDummy
        nStartDateTime = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "gseHold119"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript117"
        PBXScriptOutputTrace "  case [InsertScript117]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        
        nDiffTime = DateDiff("s", nStartDateTime, Now)
        
        If (nDiffTime = 3) Then PBXScript.Sleep 2000
        If (nDiffTime = 3) Then PBXScript.Sleep 1000
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "gseActivate120"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound118"
        PBXScriptOutputTrace "  case [PlaySound118]"
        retVal = gsePlaySoundEx2("beep.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "IMAP4_SeekMail48"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseHold119"
        PBXScriptOutputTrace "  case [gseHold119]"
        retVal = gseHoldEx("Announcements/RI_Wait.wav")
        Select Case retVal
          Case gseStateOnHold
            szState = "SetVariable165"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseActivate120"
        PBXScriptOutputTrace "  case [gseActivate120]"
        retVal = gseActivate()
        Select Case retVal
          Case gseStateActivated
            szState = "PlaySound118"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable121"
        PBXScriptOutputTrace "  case [SetVariable121]"
        rcDummy = "8"
        PBXScriptOutputTrace "exp: " & "DTMF_SMENU = ""8"""
        PBXScriptOutputTrace "res: DTMF_SMENU = " & rcDummy
        DTMF_SMENU = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "IMAP4_SeekMail20"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_GetMails122"
        PBXScriptOutputTrace "  case [IMAP4_GetMails122]"
        retVal = gseIMAP4_GetMailsEx4(gseIMAP4Var_1, True, True, False, False, nOldVoiceMailCount, "", "", OnlyFromOriginator(bUseFromCriteria), "True")
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "InsertScript124"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateIMAP4FetchCmdRunning
            szState = "gseWaitForDisconnect168"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable123"
        PBXScriptOutputTrace "  case [SetVariable123]"
        rcDummy = Now
        PBXScriptOutputTrace "exp: " & "nStartDateTime = Now"
        PBXScriptOutputTrace "res: nStartDateTime = " & rcDummy
        nStartDateTime = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "gseHold126"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript124"
        PBXScriptOutputTrace "  case [InsertScript124]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        
        DTMF_SMENU = "-1"
        
        nDiffTime = DateDiff("s", nStartDateTime, Now)
        
        If (nDiffTime = 3) Then PBXScript.Sleep 2000
        If (nDiffTime = 3) Then PBXScript.Sleep 1000
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "gseActivate127"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound125"
        PBXScriptOutputTrace "  case [PlaySound125]"
        retVal = gsePlaySoundEx2("beep.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "IMAP4_SeekMail15"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseHold126"
        PBXScriptOutputTrace "  case [gseHold126]"
        retVal = gseHoldEx("Announcements/RI_Wait.wav")
        Select Case retVal
          Case gseStateOnHold
            szState = "SetVariable163"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseActivate127"
        PBXScriptOutputTrace "  case [gseActivate127]"
        retVal = gseActivate()
        Select Case retVal
          Case gseStateActivated
            szState = "PlaySound125"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound128"
        PBXScriptOutputTrace "  case [PlaySound128]"
        retVal = gsePlaySoundEx2("beep.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "Evaluate5"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate129"
        PBXScriptOutputTrace "  case [Evaluate129]"
        PBXScriptOutputTrace "bUsePINPrompt"
        retVal = gseEvaluateEx(bUsePINPrompt)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "SetVariable112"
          Case gseStateEvaluateNoMatch
            szState = "Call131"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate130"
        PBXScriptOutputTrace "  case [Evaluate130]"
        PBXScriptOutputTrace "bUsePINPrompt"
        retVal = gseEvaluateEx(bUsePINPrompt)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "InsertScript113"
          Case gseStateEvaluateNoMatch
            szState = "Call132"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Call131"
        PBXScriptOutputTrace "  case [Call131]"
        retVal = gseCalls((InternalCall()))
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateTrue
            szState = "SetVariable112"
          Case gseStateFalse
            szState = "IMAP4_GetMails17"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Call132"
        PBXScriptOutputTrace "  case [Call132]"
        retVal = gseCalls((InternalCall()))
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateTrue
            szState = "InsertScript113"
          Case gseStateFalse
            szState = "Evaluate5"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable133"
        PBXScriptOutputTrace "  case [SetVariable133]"
        PBXUser.NumberOfNewVoicemails = 0
        PBXScriptOutputTrace "exp: " & "PBXUser.NumberOfNewVoicemails = 0"
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "IMAP4_SeekMail15"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript134"
        PBXScriptOutputTrace "  case [InsertScript134]"
        
        UseExit = 0 ' default exit
        
        Set gseIMAP4Var_1(nMailSet) = gseIMAP4Var_1(nMailSession).GetMessagesWithCallId(1,1,0,0,-1,-1,OnlyFromOriginator(bUseFromCriteria),szCallCDRID)
        PBXScript.OutputTrace "Got messages " + CStr(gseIMAP4Var_1(nMailSet).Count) + " by ID " + szCallCDRID
        If (gseIMAP4Var_1(nMailSet).Count > 0) Then
          gseIMAP4Var_1(nCurMail) = 0 ' set first mail as current
          gseIMAP4Var_1(nTotalMails) = gseIMAP4Var_1(nMailSet).Count
          gseIMAP4Var_1(nHasMailSet) = 1
          If (gseIMAP4_SaveVoicemailToTmpFileEx3(gseIMAP4Var_1, True, szVMailWav) = gseStateIMAP4Success) Then
            UseExit = 0 ' success
          End If
        Else
          UseExit = 1 ' no messages
        End If
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "gseActivate140"
          Case gseStateKey1
            szState = "PlaySound136"
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable135"
        PBXScriptOutputTrace "  case [SetVariable135]"
        rcDummy = ExtractCallCDRID(DTMF_INPUT)
        PBXScriptOutputTrace "exp: " & "szCallCDRID = ExtractCallCDRID(DTMF_INPUT)"
        PBXScriptOutputTrace "res: szCallCDRID = " & rcDummy
        szCallCDRID = rcDummy
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "DTMF_INPUT = """""
        PBXScriptOutputTrace "res: DTMF_INPUT = " & rcDummy
        DTMF_INPUT = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate141"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound136"
        PBXScriptOutputTrace "  case [PlaySound136]"
        retVal = gsePlaySoundEx2("RI_Voicemail_With_CallID_Not_Found.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "End1"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound137"
        PBXScriptOutputTrace "  case [PlaySound137]"
        retVal = gsePlaySoundEx2(szVMailWav, "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable144"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate138"
        PBXScriptOutputTrace "  case [Evaluate138]"
        PBXScriptOutputTrace "PostDialingDigits() <> """""
        retVal = gseEvaluateEx(PostDialingDigits() <> "")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "PlaySound142"
          Case gseStateEvaluateNoMatch
            szState = "Evaluate129"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseHold139"
        PBXScriptOutputTrace "  case [gseHold139]"
        retVal = gseHoldEx("Announcements/RI_Wait.wav")
        Select Case retVal
          Case gseStateOnHold
            szState = "InsertScript134"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseActivate140"
        PBXScriptOutputTrace "  case [gseActivate140]"
        retVal = gseActivate()
        Select Case retVal
          Case gseStateActivated
            szState = "PlaySound143"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate141"
        PBXScriptOutputTrace "  case [Evaluate141]"
        PBXScriptOutputTrace "Len(szCallCDRID)>0"
        retVal = gseEvaluateEx(Len(szCallCDRID)>0)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "gseHold139"
          Case gseStateEvaluateNoMatch
            szState = "PlaySound78"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound142"
        PBXScriptOutputTrace "  case [PlaySound142]"
        retVal = gsePlaySoundEx2("Announcements/1_second_silence.wav", "", False, rcDummy, True, False, "1", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "SetVariable83"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "PlaySound143"
        PBXScriptOutputTrace "  case [PlaySound143]"
        retVal = gsePlaySoundEx2("beep.wav", "", False, rcDummy, True, False, "", False)
        Select Case retVal
          Case gseStatePlayed
            szState = "PlaySound137"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable144"
        PBXScriptOutputTrace "  case [SetVariable144]"
        PBXUser.NumberOfNewVoicemails = 0
        PBXScriptOutputTrace "exp: " & "PBXUser.NumberOfNewVoicemails = 0"
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "End1"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable145"
        PBXScriptOutputTrace "  case [SetVariable145]"
        rcDummy = "0"
        PBXScriptOutputTrace "exp: " & "iPinRetries = ""0"""
        PBXScriptOutputTrace "res: iPinRetries = " & rcDummy
        iPinRetries = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "PlaySound13"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate146"
        PBXScriptOutputTrace "  case [Evaluate146]"
        PBXScriptOutputTrace "iPinRetries>2"
        retVal = gseEvaluateEx(iPinRetries>2)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "Terminate148"
          Case gseStateEvaluateNoMatch
            szState = "GetDTMFString22"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable147"
        PBXScriptOutputTrace "  case [SetVariable147]"
        rcDummy = iPinRetries+1
        PBXScriptOutputTrace "exp: " & "iPinRetries = iPinRetries+1"
        PBXScriptOutputTrace "res: iPinRetries = " & rcDummy
        iPinRetries = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate146"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Terminate148"
        PBXScriptOutputTrace "  case [Terminate148]"
        retVal = gseTerminate("0")
        Select Case retVal
          Case gseStateTerminated
            szState = "End1"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable149"
        PBXScriptOutputTrace "  case [SetVariable149]"
        rcDummy =  nMainMenuLoopCnt + 1
        PBXScriptOutputTrace "exp: " & "nMainMenuLoopCnt =  nMainMenuLoopCnt + 1"
        PBXScriptOutputTrace "res: nMainMenuLoopCnt = " & rcDummy
        nMainMenuLoopCnt = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate150"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate150"
        PBXScriptOutputTrace "  case [Evaluate150]"
        PBXScriptOutputTrace "nMainMenuLoopCnt < 3"
        retVal = gseEvaluateEx(nMainMenuLoopCnt < 3)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "GetDTMFChar8"
          Case gseStateEvaluateNoMatch
            szState = "End1"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable151"
        PBXScriptOutputTrace "  case [SetVariable151]"
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "nMainMenuLoopCnt = 0"
        PBXScriptOutputTrace "res: nMainMenuLoopCnt = " & rcDummy
        nMainMenuLoopCnt = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "GetDTMFChar8"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "IMAP4_GetMail152"
        PBXScriptOutputTrace "  case [IMAP4_GetMail152]"
        retVal = gseIMAP4_GetMailEx2(gseIMAP4Var_1, True, szFrom, False, rcDummy, False, rcDummy, False, rcDummy, False, rcDummy, False, rcDummy)
        Select Case retVal
          Case gseStateIMAP4Success
            szState = "InsertScript153"
          Case gseStateIMAP4Failed
            szState = "PlaySound32"
          Case gseStateIMAP4Error
            szState = "PlaySound4"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript153"
        PBXScriptOutputTrace "  case [InsertScript153]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        
        Dim szFromFullStr
        
        szFromFullStr = szFrom
        
        GetAddrFromFullAddrStr szFromFullStr
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "Evaluate69"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable154"
        PBXScriptOutputTrace "  case [SetVariable154]"
        rcDummy =  mGetAllVoicemails2 + 1
        PBXScriptOutputTrace "exp: " & "mGetAllVoicemails2 =  mGetAllVoicemails2 + 1"
        PBXScriptOutputTrace "res: mGetAllVoicemails2 = " & rcDummy
        mGetAllVoicemails2 = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate155"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate155"
        PBXScriptOutputTrace "  case [Evaluate155]"
        PBXScriptOutputTrace "mGetAllVoicemails2 < 12"
        retVal = gseEvaluateEx(mGetAllVoicemails2 < 12)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "IMAP4_GetMails122"
          Case gseStateEvaluateNoMatch
            szState = "End1"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable156"
        PBXScriptOutputTrace "  case [SetVariable156]"
        rcDummy =  mGetNewVoicemails + 1
        PBXScriptOutputTrace "exp: " & "mGetNewVoicemails =  mGetNewVoicemails + 1"
        PBXScriptOutputTrace "res: mGetNewVoicemails = " & rcDummy
        mGetNewVoicemails = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate157"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate157"
        PBXScriptOutputTrace "  case [Evaluate157]"
        PBXScriptOutputTrace "mGetNewVoicemails < 12"
        retVal = gseEvaluateEx(mGetNewVoicemails < 12)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "IMAP4_GetMails17"
          Case gseStateEvaluateNoMatch
            szState = "End1"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable158"
        PBXScriptOutputTrace "  case [SetVariable158]"
        rcDummy =  mGetAllEmails + 1
        PBXScriptOutputTrace "exp: " & "mGetAllEmails =  mGetAllEmails + 1"
        PBXScriptOutputTrace "res: mGetAllEmails = " & rcDummy
        mGetAllEmails = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate159"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate159"
        PBXScriptOutputTrace "  case [Evaluate159]"
        PBXScriptOutputTrace "mGetAllEmails < 12"
        retVal = gseEvaluateEx(mGetAllEmails < 12)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "IMAP4_GetMails31"
          Case gseStateEvaluateNoMatch
            szState = "End1"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable160"
        PBXScriptOutputTrace "  case [SetVariable160]"
        rcDummy =  mGetAllVoicemailsAll + 1
        PBXScriptOutputTrace "exp: " & "mGetAllVoicemailsAll =  mGetAllVoicemailsAll + 1"
        PBXScriptOutputTrace "res: mGetAllVoicemailsAll = " & rcDummy
        mGetAllVoicemailsAll = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "Evaluate161"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate161"
        PBXScriptOutputTrace "  case [Evaluate161]"
        PBXScriptOutputTrace "mGetAllVoicemailsAll < 12"
        retVal = gseEvaluateEx(mGetAllVoicemailsAll < 12)
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "IMAP4_GetMails46"
          Case gseStateEvaluateNoMatch
            szState = "End1"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable162"
        PBXScriptOutputTrace "  case [SetVariable162]"
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "mGetNewVoicemails = 0"
        PBXScriptOutputTrace "res: mGetNewVoicemails = " & rcDummy
        mGetNewVoicemails = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "IMAP4_GetMails17"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable163"
        PBXScriptOutputTrace "  case [SetVariable163]"
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "mGetAllVoicemails2 = 0"
        PBXScriptOutputTrace "res: mGetAllVoicemails2 = " & rcDummy
        mGetAllVoicemails2 = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "IMAP4_GetMails122"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable164"
        PBXScriptOutputTrace "  case [SetVariable164]"
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "mGetAllEmails = 0"
        PBXScriptOutputTrace "res: mGetAllEmails = " & rcDummy
        mGetAllEmails = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "IMAP4_GetMails31"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SetVariable165"
        PBXScriptOutputTrace "  case [SetVariable165]"
        rcDummy = 0
        PBXScriptOutputTrace "exp: " & "mGetAllVoicemailsAll = 0"
        PBXScriptOutputTrace "res: mGetAllVoicemailsAll = " & rcDummy
        mGetAllVoicemailsAll = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "IMAP4_GetMails46"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseWaitForDisconnect166"
        PBXScriptOutputTrace "  case [gseWaitForDisconnect166]"
        retVal = gseWaitForDisconnect("5")
        Select Case retVal
          Case gseStateDisconnected
            szState = "End1"
          Case gseStateTimeout
            szState = "SetVariable156"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseWaitForDisconnect167"
        PBXScriptOutputTrace "  case [gseWaitForDisconnect167]"
        retVal = gseWaitForDisconnect("5")
        Select Case retVal
          Case gseStateDisconnected
            szState = "End1"
          Case gseStateTimeout
            szState = "SetVariable158"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseWaitForDisconnect168"
        PBXScriptOutputTrace "  case [gseWaitForDisconnect168]"
        retVal = gseWaitForDisconnect("5")
        Select Case retVal
          Case gseStateDisconnected
            szState = "End1"
          Case gseStateTimeout
            szState = "SetVariable154"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "gseWaitForDisconnect169"
        PBXScriptOutputTrace "  case [gseWaitForDisconnect169]"
        retVal = gseWaitForDisconnect("5")
        Select Case retVal
          Case gseStateDisconnected
            szState = "End1"
          Case gseStateTimeout
            szState = "SetVariable160"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFChar170"
        PBXScriptOutputTrace "  case [GetDTMFChar170]"
        retVal = gseGetDTMFCharEx3("av_ri_statusOptions.wav", "0", "1", "10", True, StatusOption, True, False, rcDummy, "1234567*")
        Select Case retVal
          Case gseStateKey0
            szState = "GetDTMFChar170"
          Case gseStateKey1
            szState = "InsertScript171"
          Case gseStateKey2
            szState = "InsertScript171"
          Case gseStateKey3
            szState = "InsertScript171"
          Case gseStateKey4
            szState = "InsertScript171"
          Case gseStateKey5
            szState = "InsertScript171"
          Case gseStateKey6
            szState = "InsertScript171"
          Case gseStateKey7
            szState = "InsertScript171"
          Case gseStateKey8
            szState = "GetDTMFChar170"
          Case gseStateKey9
            szState = "GetDTMFChar170"
          Case gseStateKey10
            szState = "GetDTMFChar8"
          Case gseStateKey11
            szState = "GetDTMFChar170"
          Case gseStateTimeout
            szState = "GetDTMFChar170"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript171"
        PBXScriptOutputTrace "  case [InsertScript171]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        pbxcall.playmessage "av_ri_readStatus.wav"
        pbxcall.playmessage "av_reason"&StatusOption&".wav"
        
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "GetDTMFString172"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFString172"
        PBXScriptOutputTrace "  case [GetDTMFString172]"
        retVal = gseGetDTMFStringEx2(dateEntry, True, "av_ri_enterDate.wav", False, True, "#", "6", "3")
        Select Case retVal
          Case gseStateDTMFStringCollected
            szState = "GetDTMFString173"
          Case gseStateTimeout
            szState = "GetDTMFString173"
          Case gseStateOnlyStopChar
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFString173"
        PBXScriptOutputTrace "  case [GetDTMFString173]"
        retVal = gseGetDTMFStringEx2(timeEntry, True, "av_ri_enterTime.wav", False, True, "#", "4", "3")
        Select Case retVal
          Case gseStateDTMFStringCollected
            szState = "InsertScript177"
          Case gseStateTimeout
            szState = "SayTime174"
          Case gseStateOnlyStopChar
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SayTime174"
        PBXScriptOutputTrace "  case [SayTime174]"
        retVal = gseSayTimeEx(myTime, False, "", False, rcDummy, True)
        Select Case retVal
          Case gseStatePlayed
            szState = "SayDate175"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateFailed
            szState = "SayDate175"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "SayDate175"
        PBXScriptOutputTrace "  case [SayDate175]"
        retVal = gseSayDateEx2(myDate, False, "", False, rcDummy, True, False)
        Select Case retVal
          Case gseStatePlayed
            szState = "GetDTMFChar176"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateFailed
            szState = "GetDTMFChar176"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "GetDTMFChar176"
        PBXScriptOutputTrace "  case [GetDTMFChar176]"
        retVal = gseGetDTMFCharEx3("av_ri_confirmation.wav", "0", "1", "10", False, rcDummy, True, False, rcDummy, "01*")
        Select Case retVal
          Case gseStateKey0
            szState = "GetDTMFString172"
          Case gseStateKey1
            szState = "ConnectTo178"
          Case gseStateKey2
            szState = "GetDTMFChar170"
          Case gseStateKey3
            szState = "GetDTMFChar170"
          Case gseStateKey4
            szState = "GetDTMFChar170"
          Case gseStateKey5
            szState = "GetDTMFChar170"
          Case gseStateKey6
            szState = "GetDTMFChar170"
          Case gseStateKey7
            szState = "GetDTMFChar170"
          Case gseStateKey8
            szState = "GetDTMFChar170"
          Case gseStateKey9
            szState = "GetDTMFChar170"
          Case gseStateKey10
            szState = "GetDTMFChar170"
          Case gseStateKey11
            szState = "GetDTMFChar170"
          Case gseStateTimeout
            szState = "GetDTMFChar170"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript177"
        PBXScriptOutputTrace "  case [InsertScript177]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        Dim Month
        Dim Year
        Dim Day
        Dim Hours
        Dim Minutes
        Dim myDate
        Dim myTime
        
        year = Mid(dateEntry,5,2)
        month = Mid(dateEntry,3,2)
        day = Mid(dateEntry,1,2)
        
        if dateEntry <> "" then
        	myDate = dateEntry
        	myDate = day &"/"&month&"/"&year
        	else
        	dateEntry ="000000"
        End if
        
        if timeEntry <>"" then
        	myTime = left(timeEntry,2) & ":" & right(timeEntry,2)
        	else
        	timeEntry ="0000"
        End If
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "SayTime174"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "ConnectTo178"
        PBXScriptOutputTrace "  case [ConnectTo178]"
        retVal = gseConnectToEx6(IpPbx.CallingNumber&StatusOption&dateEntry&timeEntry, "15", "", False, rcDummy, True, False, "", True, "", True)
        Select Case retVal
          Case gseStateConnected
            Exit Do
          Case gseStateTimeout
            Exit Do
          Case gseStateNoAnswer
            Exit Do
          Case gseStateNotDelivered
            szState = "End1"
          Case gseStateDTMFKeyPressed
            Exit Do
          Case gseStateProceedWDstScr
            szState = "End1"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case Else
        'Something wrong
        Exit Do
    End Select
    
  Loop
  
  rcDummy = gseIMAP4_CloseSession(gseIMAP4Var_1)
  
  'update input parameters
  rInputParams.UpdateParamValue "bEnabled", bEnabled
  rInputParams.UpdateParamValue "szServerName", szServerName
  rInputParams.UpdateParamValue "szUserName", szUserName
  rInputParams.UpdateParamValue "szPassword", szPassword
  rInputParams.UpdateParamValue "szMailFolder", szMailFolder
  rInputParams.UpdateParamValue "bUseSSL", bUseSSL
  rInputParams.UpdateParamValue "bUsePINPrompt", bUsePINPrompt
  rInputParams.UpdateParamValue "szPIN", szPIN
  rInputParams.UpdateParamValue "bUseFromCriteria", bUseFromCriteria
  
  PBXScriptOutputTrace "End GSE Script (Action: actionStandardRemoteInquiry)"
End Sub
'' SIG '' Begin Signature
'' SIG '' IKUJNZCMLPNLQTPTWUKZOLGMEXIFVUPVVFJPXMXHHZOURLAWEVQYXUFPHUII
'' SIG '' BZYMLNBRKLUCSMLLTMZIUKEKOOYQAWKWBWXJKXZNDUUUZLYNOQZOKLAYKWLW
'' SIG '' KVHLWOVVAOVCCOKWAMWHEITKVXRHRJOEVRLQRSIVLXPHFOSLHHKLFLYPPKDO
'' SIG '' MWPUSMJCROJDGJMEMQGCNZASYOJDIOULNGMMAYTKBNFVYNPMZJONKHWPFJBY
'' SIG '' MUXYNFXBABUFCGUMADNPAPHFPYPETZXEVHOTSYIETVGPJMMKFLROMPHAFWRI
'' SIG '' JKUENXISPUKCEDERQKQVWBROLOJHCRONLRHHOMAWYNODACSEKXWMSYLAHNTP
'' SIG '' QNMYEBVKPOMCAQWVKNJTYZBPKOOQOBWTOLKVNNHVWOARANMIMBLWIKFPXRLT
'' SIG '' LTWOTKOCDYJJSNBZEMCPCAHJOVLPFNPCAZEBDBPCWTVNMOKGUBKJAETZCHFN
'' SIG '' CDRJYZZRMXMYWBOWIOMPKKFXOOSJALRUEKMKDGSNEBKYBNPWMOLXFTEYPPCJ
'' SIG '' YBLYNSDPEMMAFKMUZIMKJPDJDAFURRVLEOTKBKAQTMBHULONUUWMLRELLNLO
'' SIG '' VOBSVZFVXSOBRTVRLXPPXYLXFAACQTMEWUVOUUIKLKJUQMLYGMOSLYDFZMFP
'' SIG '' VSZLLISSLHEMWKGORTKLSKVOUCLSENZARKMRJGBKWVLHNKPPLSRNPEATHLPP
'' SIG '' JYPLNVPFSNMUQKIDFAXGIRIPATTFUAAKHOYLYYIKWMPULCWANWNIHBQEEPQA
'' SIG '' KIXZFYLVVOZRLSPMKRHYZNZVBKWRKTKFPUBLFTDPANOYOAMCLYZZOYKRWLKD
'' SIG '' ZUTLTFWOPVYJDEVOPLWTPMMMXLBHTWNICTRDMMNHJDOMONDJLYJTQNTRCQVK
'' SIG '' NLOCFPPSZCKYRXXRUQVLUGVLZHNQONYPEXGOBJYSKXNVKGMNXTVFZZQFLKVK
'' SIG '' VLCGRGIXLFONMYMQLRNETSKOODPNAAYQYBJXTWWSJXHOXASSLKNFDCRLLZNG
'' SIG '' HKDK
'' SIG '' End Signature