﻿'$$$GSEScript$$$
'GSE action: actionAutoVoicemailV2; GSEVersion: 10.40.0.5
Sub actionAutoVoicemailV2(ByRef rInputParams)
  PBXScriptOutputTrace "Begin GSE Script (Action: actionAutoVoicemailV2)"
  PBXScriptOutputTrace "GSEVersion: 10.40.0.5"
  Dim szState, retVal, rcDummy, bWasDisconnect, UseExit
  szState = "Start0"
  bWasDisconnect = False
  
  Dim femaleVoice, avLanguage, DateString, nightService, greetPt2, leaveMessage, voicePrefix, Reason1, dateMessage, nReturnResult, pdd, Reason2, avLangPrefix, shortMessage, Reason3, Reason4, monthMessage, Reason5, enterDTMF, ReasonMessage, Reason6, greetingmessage, timeString, Reason7
  
  femaleVoice = ""
  avLanguage = ""
  DateString = ""
  nightService = ""
  greetPt2 = ""
  leaveMessage = ""
  voicePrefix = ""
  Reason1 = ""
  dateMessage = ""
  nReturnResult = gseStateTrue
  pdd = ""
  Reason2 = ""
  avLangPrefix = ""
  shortMessage = ""
  Reason3 = ""
  Reason4 = ""
  monthMessage = ""
  Reason5 = ""
  enterDTMF = ""
  ReasonMessage = ""
  Reason6 = ""
  greetingmessage = ""
  timeString = ""
  Reason7 = ""
  
  PBXScriptOutputTrace "Input parameters:"
  'extract input parameters
  femaleVoice = rInputParams.GetParamValueByName("femaleVoice", "No")
  PBXScriptOutputTrace "femaleVoice = " & femaleVoice
  avLanguage = rInputParams.GetParamValueByName("avLanguage", "GB")
  PBXScriptOutputTrace "avLanguage = " & avLanguage
  shortMessage = rInputParams.GetParamValueByName("shortMessage", "Yes")
  PBXScriptOutputTrace "shortMessage = " & shortMessage
  Reason1 = rInputParams.GetParamValueByName("Reason1", "Training")
  PBXScriptOutputTrace "Reason1 = " & Reason1
  Reason2 = rInputParams.GetParamValueByName("Reason2", "Away")
  PBXScriptOutputTrace "Reason2 = " & Reason2
  Reason3 = rInputParams.GetParamValueByName("Reason3", "Lunch")
  PBXScriptOutputTrace "Reason3 = " & Reason3
  Reason4 = rInputParams.GetParamValueByName("Reason4", "Meeting")
  PBXScriptOutputTrace "Reason4 = " & Reason4
  Reason5 = rInputParams.GetParamValueByName("Reason5", "Holiday")
  PBXScriptOutputTrace "Reason5 = " & Reason5
  Reason6 = rInputParams.GetParamValueByName("Reason6", "Out")
  PBXScriptOutputTrace "Reason6 = " & Reason6
  Reason7 = rInputParams.GetParamValueByName("Reason7", "Unavailable")
  PBXScriptOutputTrace "Reason7 = " & Reason7
  
  Do
    Select Case szState
    
      '////////////////////////
      Case "Start0"
        PBXScriptOutputTrace "  case [Start0]"
        retVal = gseStart()
        Select Case retVal
          Case gseStateStarted
            szState = "SetVariable3"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "End1"
        PBXScriptOutputTrace "  case [End1]"
        rInputParams.m_FunctionResult = nReturnResult
        Exit Do
      
      '////////////////////////
      Case "Disconnect2"
        PBXScriptOutputTrace "  case [Disconnect2]"
        If (Not bWasDisconnect) Then
          szState = "End1"
          bWasDisconnect = True
        Else
          PBXScriptOutputTrace "GSE anti-LOOP script executed"
          Exit Do
        End If
      
      '////////////////////////
      Case "SetVariable3"
        PBXScriptOutputTrace "  case [SetVariable3]"
        rcDummy = "av_reason2.wav"
        PBXScriptOutputTrace "exp: " & "ReasonMessage = ""av_reason2.wav"""
        PBXScriptOutputTrace "res: ReasonMessage = " & rcDummy
        ReasonMessage = rcDummy
        rcDummy = "av_"&CurDay()&"d.wav"
        PBXScriptOutputTrace "exp: " & "dateMessage = ""av_""&CurDay()&""d.wav"""
        PBXScriptOutputTrace "res: dateMessage = " & rcDummy
        dateMessage = rcDummy
        rcDummy = "av_"&CurMonth()&"m.wav"
        PBXScriptOutputTrace "exp: " & "monthMessage = ""av_""&CurMonth()&""m.wav"""
        PBXScriptOutputTrace "res: monthMessage = " & rcDummy
        monthMessage = rcDummy
        rcDummy = "av_hello_"&CurDayOfWeek()&".wav"
        PBXScriptOutputTrace "exp: " & "greetingmessage = ""av_hello_""&CurDayOfWeek()&"".wav"""
        PBXScriptOutputTrace "res: greetingmessage = " & rcDummy
        greetingmessage = rcDummy
        rcDummy = "Off"
        PBXScriptOutputTrace "exp: " & "nightService = ""Off"""
        PBXScriptOutputTrace "res: nightService = " & rcDummy
        nightService = rcDummy
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "voicePrefix = """""
        PBXScriptOutputTrace "res: voicePrefix = " & rcDummy
        voicePrefix = rcDummy
        rcDummy = "av_greeting1.wav"
        PBXScriptOutputTrace "exp: " & "greetPt2 = ""av_greeting1.wav"""
        PBXScriptOutputTrace "res: greetPt2 = " & rcDummy
        greetPt2 = rcDummy
        rcDummy = "av_noCli.wav"
        PBXScriptOutputTrace "exp: " & "enterDTMF = ""av_noCli.wav"""
        PBXScriptOutputTrace "res: enterDTMF = " & rcDummy
        enterDTMF = rcDummy
        rcDummy = "av_leave_message.wav"
        PBXScriptOutputTrace "exp: " & "leaveMessage = ""av_leave_message.wav"""
        PBXScriptOutputTrace "res: leaveMessage = " & rcDummy
        leaveMessage = rcDummy
        rcDummy = "No Time"
        PBXScriptOutputTrace "exp: " & "timeString = ""No Time"""
        PBXScriptOutputTrace "res: timeString = " & rcDummy
        timeString = rcDummy
        rcDummy = "No Date"
        PBXScriptOutputTrace "exp: " & "DateString = ""No Date"""
        PBXScriptOutputTrace "res: DateString = " & rcDummy
        DateString = rcDummy
        rcDummy = ""
        PBXScriptOutputTrace "exp: " & "avLangPrefix = """""
        PBXScriptOutputTrace "res: avLangPrefix = " & rcDummy
        avLangPrefix = rcDummy
        rcDummy = PostDialingDigits()
        PBXScriptOutputTrace "exp: " & "pdd = PostDialingDigits()"
        PBXScriptOutputTrace "res: pdd = " & rcDummy
        pdd = rcDummy
        
        retVal = gseStateSetVariableNext
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateSetVariableNext
            szState = "InsertScript13"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript4"
        PBXScriptOutputTrace "  case [InsertScript4]"
        
        UseExit = 0 ' default exit
        
        '
        ' Using regular expressions this code looks for Date and/or Time information in the user free status text
        ' ********************************************************************************************************
        
        UseExit = 0
        
        dim re,re2,re_result,re2_result
        dim testText
        
        ' Regular Expression to find a Date String
        Set re = New RegExp
        	re.pattern = "\d{2}\/\d{2}\/\d{2}"
        		
        testText = IpPbx.UserFreeStatusText
        	pbxscript.outputtrace "++++++++++++++++++++++++++++++++++++++++++++++++++++ User test text read as " & testText
        set re_result = re.execute(testText)
        
        if re.test(testText) then
        	pbxscript.outputtrace "++++++++++++++++++++++++++++++++++++++++++++++++++++ Match found for date in string"
        		dateString = re_result(0).value
        	else
        	pbxscript.outputtrace "++++++++++++++++++++++++++++++++++++++++++++++++++++  No Match found for Date"	
        		dateString = "No Date"
        end if
        
        ' Regular Expression to find a Time String
        Set re2 = New RegExp
        	re2.pattern = "\d{2}\:\d{2}" 
        
        set re2_result = re2.execute(testText)
        
        if re2.test(testText) then
        	pbxscript.outputtrace "++++++++++++++++++++++++++++++++++++++++++++++++++++ Match found for time in string"
        		timeString = re2_result(0).value
        	else
        		timeString = "No Time"
        end if
        pbxscript.outputtrace "************** timeString = " & timeString
        pbxscript.outputtrace "************** dateString = " & dateString
        
        
        
        
        
        ' This section builds the announcements if required to say when the user will be back at their desks
        dim timeDateMessage
        
        
        
        If dateString<> "No Date" and timeString = "No Time" then
        	timeDateMessage =  avLangPrefix&"av_back_at_my_desk_on.wav"
        	if femaleVoice="Yes" then
        		timeDateMessage="f"&timeDateMessage
        	end if
        	pbxcall.PlayMessage timeDatemessage
        	retVal=gseSayDateEx2(dateString, False, "", False, rcDummy, True, False)
        end if
        
        If dateString = "No Date" and timeString <> "No Time" then
        	timeDateMessage =  avLangPrefix&"av_backAtMyDesk.wav"
        	if femaleVoice="Yes" then
        		timeDateMessage="f"&timeDateMessage
        	end if
        	pbxcall.PlayMessage timeDatemessage
        	retVal=gseSayTimeEx(timeString, False, "", False, rcDummy, True)
        end if
        
        If dateString <> "No Date" and timeString <> "No Time" then
        	timeDateMessage =  avLangPrefix&"av_back_at_my_desk_on.wav"
        	if femaleVoice="Yes" then
        		timeDateMessage="f"&timeDateMessage
        	end if
        	pbxcall.PlayMessage timeDatemessage
        	retVal=gseSayDateEx2(dateString, False, "", False, rcDummy, True, False)
        	retVal=gseSayTimeEx(timeString, False, "", False, rcDummy, True)
        end if
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "Voicemail8"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript5"
        PBXScriptOutputTrace "  case [InsertScript5]"
        
        UseExit = 0 ' default exit
        
        
        UseExit = 0 
        
        	if NotLoggedIn() and len(IpPbx.UserFreeStatusText)<5then
        		reasonMessage = avLangPrefix&"av_reason6.wav"
        		
        		ElseIF InStr(1,IpPbx.UserFreeStatusText,Reason3,1)>0 then
        			ReasonMessage =avLangPrefix&"av_reason3.wav"
        			pbxscript.outputtrace "User at Lunch so reason message = " & ReasonMessage
        		
        		elseIf InStr(1,IpPbx.UserFreeStatusText,Reason6,1)>0 then
        			ReasonMessage = avLangPrefix&"av_reason6.wav"
        			pbxscript.outputtrace "status says out so reason message = " & ReasonMessage
        		
        		ElseIf IsOutlookBusy() or InStr(1,IpPbx.UserFreeStatusText,Reason4,1)>0 then
        			ReasonMessage =avLangPrefix&"av_reason4.wav"
        			pbxscript.outputtrace "Users Calandar shows busy or status text shows Meeting so reason message = " & ReasonMessage
        	
        		ElseIf IpPbx.DoNotDisturb and len(IpPbx.UserFreeStatusText)<5 then
        				ReasonMessage =avLangPrefix&"av_reason4.wav"
        				pbxscript.outputtrace "User in DND so reason message = " & ReasonMessage
        				
        		ElseIf IsOutlookBusy() then
        				ReasonMessage =avLangPrefix&"av_reason4.wav"
        				pbxscript.outputtrace "Users Calandar shows as busy so reason message = " & ReasonMessage
        				
        		ElseIf InStr(1,IpPbx.UserFreeStatusText,Reason7,1)>0 then
        			ReasonMessage = avLangPrefix&"av_reason7.wav"
        			pbxscript.outputtrace "status says unavailable so reason message = " & ReasonMessage
        		
        		ElseIF InStr(1,IpPbx.UserFreeStatusText,Reason1,1)>0 then
        			ReasonMessage =avLangPrefix&"av_reason1.wav"
        			
        		ElseIF InStr(1,IpPbx.UserFreeStatusText,Reason2,1)>0  then
        			ReasonMessage =avLangPrefix&"av_reason2.wav"
        			
        		ElseIF InStr(1,IpPbx.UserFreeStatusText,Reason5,1)>0  then
        			ReasonMessage =avLangPrefix&"av_reason5.wav"
        			
        		ElseIF len(IpPbx.UserFreeStatusText)=0 and IsAway() then
        			ReasonMessage =avLangPrefix&"av_reason2.wav"
        		
        	end if
        
        if femaleVoice="Yes" then
        ReasonMessage = "f" & ReasonMessage
        end if
        
        
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "InsertScript9"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript6"
        PBXScriptOutputTrace "  case [InsertScript6]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        If IsDoNotDisturb()= False then
         IpPbx.Away=True
        End if
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "End1"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript7"
        PBXScriptOutputTrace "  case [InsertScript7]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        if femaleVoice = "Yes" then
        	greetingmessage ="fav_hello_"&CurDayOfWeek()&".wav"
        	dateMessage = "fav_"&CurDay()&"d.wav"
        	monthMessage = "fav_"&Month(now)&"m.wav"
        	greetPt2 ="fav_greeting1.wav"
        	enterDTMF ="fav_noCli.wav"
        	leaveMessage = "fav_leave_message.wav"
        end if
        
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "Evaluate12"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Voicemail8"
        PBXScriptOutputTrace "  case [Voicemail8]"
        retVal = gseVoicemailEx7(False, "Default Welcome.wav", True, enterDTMF, True, leaveMessage, "120", PBXUser.VoicemailEMailAddress, "", False, rcDummy, False, True, "", "", "", True, "INBOX", False, True, "", True, False, False)
        Select Case retVal
          Case gseStateTimeout
            szState = "InsertScript6"
          Case gseStateRecorded
            szState = "InsertScript6"
          Case gseStateIMAP4Success
            szState = "InsertScript6"
          Case gseStateIMAP4Error
            szState = "InsertScript6"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript9"
        PBXScriptOutputTrace "  case [InsertScript9]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        pbxcall.playmessage greetingmessage
        pbxcall.playmessage datemessage
        pbxcall.playmessage monthmessage
        pbxcall.playmessage greetPt2
        pbxcall.playmessage reasonmessage
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "InsertScript4"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript10"
        PBXScriptOutputTrace "  case [InsertScript10]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        Select Case avLanguage
        	Case "FR"
        	avLangPrefix="FR"
        	Case "DE"
        	avLangPrefix="DE"
        	Case "SV"
        	avLangPrefix="SV"
        End Select
        
        reasonMessage = avLangPrefix&reasonMessage
        dateMessage = avLangPrefix&datemessage
        monthMessage =avLangPrefix&monthMessage
        greetingMessage = avLangPrefix&greetingMessage
        greetPt2 = avLangPrefix&greetPt2
        enterDTMF = avLangPrefix&enterDTMF
        leaveMessage=avLangPrefix&leaveMessage
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "InsertScript7"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript11"
        PBXScriptOutputTrace "  case [InsertScript11]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        
        pbxcall.playmessage greetPt2
        
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "InsertScript4"
          Case gseStateKey1
            Exit Do
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Evaluate12"
        PBXScriptOutputTrace "  case [Evaluate12]"
        PBXScriptOutputTrace "shortMessage=""Yes"""
        retVal = gseEvaluateEx(shortMessage="Yes")
        CheckIfCallIsDisconnected retVal
        Select Case retVal
          Case gseStateEvaluateMatch
            szState = "InsertScript11"
          Case gseStateEvaluateNoMatch
            szState = "InsertScript5"
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "InsertScript13"
        PBXScriptOutputTrace "  case [InsertScript13]"
        
        UseExit = 0 ' default exit
        
        ' TODO: Insert you script code here
        
        UseExit = 0 ' Please use the variable UseExit for the block exits 0..9
        dim reasonPart
        dim datePart
        dim timePart
        dim reasonText
        Dim sMonth
        Dim sYear
        Dim sDay
        Dim sHours
        Dim sMinutes
        Dim myDate
        Dim fillerText1
        Dim fillerText2
        Dim writeTime
        Dim writeDate
        Dim myReason
        	if pdd<>"" then
        
        		UseExit=1
        		reasonPart = Mid(pdd,1,1)
        		datePart = Mid(pdd,2,6)
        		timePart = Mid(pdd,8,4)
        	select case reasonPart
        		Case 1
        		myReason = Reason1
        		case 2
        		myReason = Reason2
        		case 3
        		myReason = Reason3
        		case 4 
        		myreason = Reason4
        		case 5
        		myReason = Reason5
        		case 6
        		myReason = Reason6
        		case 7
        		myReason = Reason7
        	end select
        		
        	fillerText1=" Returning "
        	fillerText2=""
        
        	sYear = Mid(pdd,6,2)
        	sMonth = Mid(pdd,4,2)
        	sDay = Mid(pdd,2,2)
        	myDate = mid(pdd,2,6)
        	sMinutes = mid(pdd,8,2)
        	sHours = mid(pdd,10,2)
        
        	if sMinutes ="00" and sHours ="00" then
        		writeTime=""
        		else
        		writeTime = sMinutes & ":" & sHours
        		fillerText2 = " at "
        	end if
        
        	if myDate ="000000" then
        		writeDate=""
        		else
        		writeDate= sDay & "/" & sMonth & "/" & sYear
        		fillerText1 = " Returning on "
        	end if
        
        	
        
        	IpPbx.UserFreeStatusText=myReason&fillerText1&writeDate&fillerText2&writeTime
        End If
        
        
        Select Case CStr(UseExit)
          Case "1"
            retVal = gseStateKey1
          Case "2"
            retVal = gseStateKey2
          Case "3"
            retVal = gseStateKey3
          Case "4"
            retVal = gseStateKey4
          Case "5"
            retVal = gseStateKey5
          Case "6"
            retVal = gseStateKey6
          Case "7"
            retVal = gseStateKey7
          Case "8"
            retVal = gseStateKey8
          Case "9"
            retVal = gseStateKey9
          Case Else
            retVal = gseStateKey0
        End Select
        
        CheckIfCallIsDisconnected retVal
        
        Select Case retVal
          Case gseStateKey0
            szState = "InsertScript10"
          Case gseStateKey1
            szState = "Terminate14"
          Case gseStateKey2
            Exit Do
          Case gseStateKey3
            Exit Do
          Case gseStateKey4
            Exit Do
          Case gseStateKey5
            Exit Do
          Case gseStateKey6
            Exit Do
          Case gseStateKey7
            Exit Do
          Case gseStateKey8
            Exit Do
          Case gseStateKey9
            Exit Do
          Case gseStateDisconnected
            szState = "Disconnect2"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case "Terminate14"
        PBXScriptOutputTrace "  case [Terminate14]"
        retVal = gseTerminate("0")
        Select Case retVal
          Case gseStateTerminated
            szState = "End1"
          Case Else
            PBXScriptOutputTrace "Error: Abnormal case thread"
            Exit Do
        End Select
      
      '////////////////////////
      Case Else
        'Something wrong
        Exit Do
    End Select
    
  Loop
  
  
  'update input parameters
  rInputParams.UpdateParamValue "femaleVoice", femaleVoice
  rInputParams.UpdateParamValue "avLanguage", avLanguage
  rInputParams.UpdateParamValue "shortMessage", shortMessage
  rInputParams.UpdateParamValue "Reason1", Reason1
  rInputParams.UpdateParamValue "Reason2", Reason2
  rInputParams.UpdateParamValue "Reason3", Reason3
  rInputParams.UpdateParamValue "Reason4", Reason4
  rInputParams.UpdateParamValue "Reason5", Reason5
  rInputParams.UpdateParamValue "Reason6", Reason6
  rInputParams.UpdateParamValue "Reason7", Reason7
  
  PBXScriptOutputTrace "End GSE Script (Action: actionAutoVoicemailV2)"
End Sub
'' SIG '' Begin Signature
'' SIG '' CZNOPWUEKEKSTFPLODCTQTMEFLZIZWSKVTKOYLOKKKMDGULOKLHVADCNZSUK
'' SIG '' UTUONGDYHGSUNOZLHALWZFSMNBFSTKRFUTQNIFPLVGKEHGGUMNKLJHOIFMLM
'' SIG '' CKKSDDFONCGFFNPVOKQEBYLUCMHBLGFYJOWGIWENBQXMGBXONDJCCLKWMXLZ
'' SIG '' QLZUYHRGVUIGJPOWAVDLQHTOGGZOJVMIZDIPAFIEFTCKPOKTMKROGYVNWEFN
'' SIG '' ACHDWKXIBYFEPMQWNETEUKOQXAUDEPECEWNKOCPNTOEUXCFRNZMKUIIMWFRG
'' SIG '' RURTDOOWCPJSPFCIOAVVZWDZQSKOPOGVKOYIZZXMHRLFKREXGCJJYYUWNCWJ
'' SIG '' DTGXOLPSFKCVOMFDMENLPHNSPSPIOYFKLZIRFDZPGYXYPJRQLTFLYPHPETJU
'' SIG '' OXGRMMKIWNVAKWSULAIPOCJDOBGPKODAWNPWXWLZMONPROEJARIKKNMKNUIY
'' SIG '' KMDVMLKRULWHJAIEGCNIWEIORPILBMQKKPCLWWLMTPKIVQPLSIFOOUOFIMDH
'' SIG '' FABDMLRVKIIHNQAEESMKAPFCOHSQNIMDARRQULOYQAXAENPPPNHZURLHPBQO
'' SIG '' SRWMAXBVCRRXFCMOZNNMYLRWKAHNRUUDSACPRRYXXIVOQWSMGKPEXGAMMKJA
'' SIG '' MFQONKJMELOENMQNNSDAJAZPOLDLDVCYEPAQEXENZTBXWDLUYJQAELSJYLDG
'' SIG '' LMKGOMBNRKKBKFJFCKTBAXVGZOVKHBNYWMPIFUAYMLWNQXDAWATHGKDSLXTL
'' SIG '' MVRUUQYPKDDGQEFUCQQWZNQIPKWLLMRLLCINWAIFUPCBAVEYOMLKNKGASDGB
'' SIG '' BPQMLMXYKFMMVENLNIAKQSJNKFEMYYOMPBCPTDOOCTPELNZPNKBEDNFODOFN
'' SIG '' WPNXBGAUNKXPXGFLXFMGXOIMMZLPOPZVXKKULQNAXKQOOYZMODLJNEYLENNU
'' SIG '' JHGWLWOLARSARPYPAFRJMMDMLOFKMMAOESLUMNNNUTDNEWDFPPKPWUZRMHQK
'' SIG '' NSTS
'' SIG '' End Signature